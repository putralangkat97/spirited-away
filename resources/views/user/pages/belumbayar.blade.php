@extends('user.layouts.master')

@section('content')
    <div class="row">
        <div class="container">
            <div class="col-xl-12">
                <div class="card-box">
                    @if($belum)
                    <div class="card-body">
                        <div class="alert alert-danger">
                            <strong>{{ $belum }}</strong>
                        </div>
                        <h4>
                            <p class="lead">Silahkan kembali kehalaman dashboard</p>
                            <p><a href="{{ route('user.dashboard') }}" class="btn btn-primary">Kembali</a></p>
                        </h4>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection