@extends('admin.layouts.master')

@section('title', 'Edit Pertanyaan')

@section('page-title', 'Edit Pertanyaan')

@section('content')
    <div class="row">
        <div class="col-xl-12">
            <div class="card-box">
                <h4 class="header-title mb-2">Kategori: <strong>{{ $pertanyaans->quiz->olimpiade->nama_olimpiade }} {{ $pertanyaans->quiz->olimpiade->tingkat }} ({{ $pertanyaans->quiz->kode_soal }})</strong></h4>
                <hr>
                <h4 class="header-title mt-2">Ubah Pertanyaan:</h4>
                <form action="{{ route('admin.pertanyaanUpdate') }}" method="post">
                @csrf
                @method('PUT')
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <textarea name="pertanyaan" id="summernote" class="form-control">{{ $pertanyaans->pertanyaan }}</textarea>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="id" value="{{ $pertanyaans->id }}">
                    <input type="hidden" name="quiz_id" value="{{ $pertanyaans->quiz->id }}">
                    <input type="submit" value="Update" class="btn btn-rounded btn-primary">
                    <a href="{{ route('admin.pertanyaan', $pertanyaans->quiz->id) }}" class="btn btn-rounded btn-warning">Batal</a>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script>
    CKEDITOR.replace('summernote', {
        filebrowserImageBrowseUrl: '/filemanager?type=Images',
        filebrowserImageUploadUrl: '/filemanager/upload?type=Images&_token=',
        filebrowserBrowseUrl: '/filemanager?type=Files',
        filebrowserUploadUrl: '/filemanager/upload?type=Files&_token='
    });
</script>
@endsection