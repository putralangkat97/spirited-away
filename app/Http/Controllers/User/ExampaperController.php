<?php

namespace App\Http\Controllers\User;

use App\User;
use Carbon\Carbon;
use App\Models\Quiz;
use App\Models\Result;
use App\Models\Jawaban;
use App\Models\UserLog;
use App\Models\Olimpiade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ExampaperController extends Controller
{
    public function __construct() {
        $this->middleware('auth:web');
    }

    public function exampaper(Request $request) {
        $kode = $request->get('cari');
        $dateStart = Olimpiade::where('is_active', 1)->get()->map->only('start_date', 'end_date')->toArray();
        $quizz = Quiz::where('kode_soal', 'like', '%'. $kode .'%')->get()->pluck('olimpiade_id');
        if (!empty($dateStart)) {
            if (now('Asia/Jakarta') > $dateStart[0]['end_date']) {
                // dd('event telah berakhir');
                return redirect()->route('user.berakhir')->with('berakhir', 'Event telah berakhir.');
            }

            $Quizid = Quiz::where('kode_soal', 'like', '%'.$kode.'%')->pluck('olimpiade_id')->toArray();
            $users = User::where('id', Auth::user()->id)->pluck('id')->toArray();
            
            $results = Result::where('user_id', Auth::user()->id)->get()->pluck('user_id')->toArray();
            $tes = DB::table('olimpiade_user')->where('olimpiade_id', $Quizid)
                ->where('user_id', $users)
                ->pluck('user_id')
                ->toArray();
            $tess = DB::table('olimpiade_user')->where('olimpiade_id', $Quizid)->where('user_id', $users)->pluck('olimpiade_id')->toArray();
    
            if ($users == $tes && $Quizid == $tess) {
                if ($users == $results) {
                    return view('user.pages.resultsError')->with('error', 'Maaf yaa.. Kamu sudah mengikuti ujian ini');
                } else {
                    $quizzes = Quiz::where('kode_soal', 'like', '%'.$kode.'%')->with('olimpiade')->get();
                    return view('user.pages.exampaper')->with('quizzes', $quizzes);
                }
            } else {
                return redirect()->route('user.notfound')->with('notfound', 'Kamu tidak terdaftar di kategori soal ini, mohon hubungi admin untuk info lebih lanjut');
            }
        }

        return redirect()->route('user.kosong')->with('kosong', 'Maaf, event ini belum tersedia.');
    }

    public function notfound() {
        return view('user.pages.notfound');
    }

    public function kosong() {
        return view('user.pages.eventKosong');
    }

    public function berakhir() {
        return view('user.pages.eventBerakhir');
    }

    public function lembarsoal($kode_soal) {
        $soals = Quiz::where('kode_soal', $kode_soal)->with([
            'pertanyaans' => function ($q) {
                return $q->inRandomOrder()->with([
                    'jawabans' => function ($q) {
                        return $q->inRandomOrder();
                    }
                ]);
            }
        ])->with('olimpiade')->get();

        $waktu = Quiz::where('kode_soal', $kode_soal)->pluck('waktu');
        // dd($waktu);

        return view('user.pages.lembarsoal')->with(['soals' => $soals, 'waktu' => $waktu]);
    }

    public function storeTest(Request $request) {
        $uid = Auth::user()->id;
        $tess = array(
            'jawaban_id' => $request->answer,
        );

        $point = '';
        $userid = User::where('id', $uid)->pluck('id')->toArray();  
        if ($result = Result::where('user_id', $userid)->where('olimpiade_id', $request->olimpiade_id)->first() !== null) {
            return view('user.pages.resultsError')->with('error', 'Maaf yaa.. Kamu sudah mengikuti ujian ini');
        } else {
            if ($request->answer == null) {
                $point = 0;
                $userlogs = UserLog::create([
                    'user_id' => $uid,
                    'olimpiade_id' => $request->olimpiade_id,
                    'point' => $point
                ]);
            } else {
                $jawaban = Jawaban::find(array_values($request->answer));
                $point = $jawaban->sum('point');
                foreach ($tess['jawaban_id'] as $key => $val) {
                    $result = Result::create([
                        'pertanyaan_id' => $key,
                        'jawaban_id' => $val,
                        'user_id' => $uid,
                        'logged_in' => $request->waktu,
                        'olimpiade_id' => $request->olimpiade_id
                    ]);
                }
                $user = User::where('id', $uid)->update(['jumlah_point' => $point]);
                $userlogs = UserLog::create([
                    'user_id' => $uid,
                    'olimpiade_id' => $request->olimpiade_id,
                    'point' => $point
                ]);
                // $user = User::where('id', $uid)->increment('jumlah_point', $point);
            }

            return view('user.pages.results', compact(/*'benar', 'salah', */'point'))->with('success', 'Yeay! Kamu berhasil menyelesaikan ujian nya. lihat score kamu dibawah');
        }
            
            // $benar = 0;
            // $salah = 0;
            // $data = Result::where('user_id', Auth::user()->id)->pluck('id');
            // $oidd = Result::pluck('olimpiade_id')->toArray();
            // foreach ($data as $key => $value) {
            //     $results = Result::where([['id', '=', $value], ['olimpiade_id', '=', $oidd]])->get();
            //     dump($results);
            //     // $true = Jawaban::where('correct', 'Y')->where('id', $results[0]['jawaban_id'])->select('correct')->count();
            //     // $false = Jawaban::where('correct', 'N')->where('id', $results[0]['jawaban_id'])->select('correct')->count();
            //     // $salah += $false;
            //     // $benar += $true;
            // }
    }
}
