@extends('admin.layouts.master')

@section('title', 'Tambah User')

@section('page-title', 'Tambah User')

@section('content')
    <div class="row">
        <div class="col-xl-12">
            <div class="card-box">
                <h4 class="header-title">Form User:</h4>
                <p class="sub-header"><em>Semua form dan option harus diisi.</em></p>
                <form action="{{ route('admin.userInsert') }}" id="userForm" method="post" data-parsley-validate>
                @csrf
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="nama_lengkap">Nama Lengkap:</label>
                                <input type="text" name="nama_lengkap" id="nama_lengkap" class="form-control" required placeholder="Nama Lengkap" data-parsley-required-message="Nama tidak boleh kosong.">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="jenis_kelamin">Jenis Kelamin:</label>
                                <select name="jenis_kelamin" id="jenis_kelamin" class="form-control" required data-parsley-required-message="Harus pilih Jenis kelamin.">
                                    <option value="">Pilih</option>
                                    <option value="Laki-Laki">Laki-Laki</option>
                                    <option value="Perempuan">Perempuan</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="asal_sekolah">Asal Sekolah:</label>
                                <input type="text" name="asal_sekolah" id="asal_sekolah" class="form-control" required placeholder="Asal Sekolah" data-parsley-required-message="Asal sekolah tidak boleh kosong.">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="kelas">Kelas:</label>
                                <select name="kelas" id="kelas" class="form-control" required  data-parsley-required-message="Harus pilih Kelas.">
                                    <option value="">Pilih</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="olimpiade_id">Pilihan Olimpiade:</label>
                                <select id="olimpiade_id" name="olimpiade_id[]" multiple="multiple" class="js-example-basic-multiple form-control" required data-parsley-required-message="Pilihan olimpiade tidak boleh kosong.">
                                    @foreach($olimpiades as $key => $olimpiade)
                                    <option value="{{ $olimpiade->id }}">{{ $olimpiade->nama_olimpiade }} {{ $olimpiade->tingkat }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="provinsi_id">Provinsi:</label>
                                <select id="provinsi_id" name="provinsi_id" class="form-control" required data-parsley-required-message="Harus pilih Provinsi.">
                                <option value="">Pilih</option>
                                @foreach($provinces as $id => $name)
                                <option value="{{ $id }}">{{ $name }}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="kabupaten_kota_id">Kota/Kabupaten</label>
                                <select name="kabupaten_kota_id" id="kabupaten_kota_id" class="form-control" required data-parsley-required-message="Harus pilih kabupaten/kota.">
                                    <option value="">Pilih</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="username">Username <span class="text-muted"><em>(tidak boleh memakai spasi)</em></span>:</label>
                                <input type="text" name="username" id="username" class="form-control" required placeholder="username" data-parsley-required-message="username tidak boleh kosong.">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="email">E-mail <span class="text-muted"><em>(tidak boleh memakai spasi)</em></span>:</label>
                                <input type="email" name="email" id="email" class="form-control" required placeholder="main@example.com" data-parsley-required-message="E-mail tidak boleh kosong." data-parsley-type="email">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="password">Password <span class="text-muted"><em>(minimal 6 karakter)</em></span>:</label>
                                <input type="password" name="password" id="password" class="form-control" required placeholder="password" data-parsley-required-message="password tidak boleh kosong">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="confirm">Konfirmasi Password:</label>
                                <input type="password" name="confirm" id="confirm" class="form-control" required placeholder="confirm password" onchange="check();" data-parsley-required-message="Konfirmasi password tidak boleh kosong">
                                <span id="tes"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="tanggal_lahir">Tanggal Lahir:</label>
                                <input type="text" name="tanggal_lahir" id="tanggal_lahir" class="form-control datepicker" required placeholder="Tanggal Lahir" data-parsley-required-message="Tanggal lahir tidak boleh kosong">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="tempat_lahir">Tempat Lahir:</label>
                                <input type="text" name="tempat_lahir" id="tempat_lahir" class="form-control" required placeholder="Tempat Lahir" data-parsley-required-message="Tempat lahir harus diisi">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="alamat">Alamat:</label>
                                <textarea name="alamat" id="alamat" rows="4" class="form-control" required placeholder="Alamat" data-parsley-required-message="Alamat tidak boleh kosong."></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="no_telepon_whatsapp">No. Telp/Whatsapp <span class="text-muted"><em>(Number Only)</em></span>:</label>
                                <input type="text" name="no_telepon_whatsapp" id="no_telepon_whatsapp" class="form-control" required placeholder="e.g. 08123456" data-parsley-required-message="No. Telp tidak boleh kosong." oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="is_active">Status:</label>
                                <select name="is_active" id="is_active" class="form-control" required data-parsley-required-message="Harus pilih salah satu.">
                                    <option value="">Pilih</option>
                                    <option value="1">Aktif</option>
                                    <option value="0">Tidak Aktif</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <input class="btn btn-primary btn-block" type="submit" id="action" value="Tambah">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
<script src="{{asset('js/select2.min.js')}}"></script>
<script src="{{asset('js/gijgo.min.js')}}"></script>

<script>
    $(document).ready(function() {
        $('.js-example-basic-multiple').select2();
        $('.datepicker').datepicker({
            uiLibrary: 'bootstrap',
            format: 'dd-mm-yyyy'
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        });

        $(document).on('change', '#provinsi_id', function () {
            $.post('{{ route("admin.kota") }}', { id: $(this).val() }).then(function (response) {
                // console.log(response);
                $('#kabupaten_kota_id').empty();

                $.each(response, function (id, name) {
                    $('#kabupaten_kota_id').append(new Option(name, id));
                });
            });
        });
    });

    function check() {
        var pass = $('#password').val();
        var conpass = $('#confirm').val();

        if (pass != conpass) {
            $('#tes').html('Password tidak cocok!').addClass('text-danger');
        } else {
            $('#tes').html('Password cocok').removeClass('text-danger').addClass('text-success');
        }
    }

    $(document).ready(function () {
        $('#confirm').keyup(check);

        $("input#username").on({
            keydown: function(e) {
                if (e.which === 32)
                return false;
            },
            change: function() {
                this.value = this.value.replace(/\s/g, "");
            }
        });
    })
</script>
@endsection
