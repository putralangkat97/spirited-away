@extends('admin.layouts.master')

@section('title', 'Edit Jawaban')

@section('page-title', 'Edit Jawaban')

@section('content')
    <div class="row">
        <div class="col-xl-12">
            <div class="card-box">
                <h4 class="header-title mb-2">Pertanyaan:</h4>
                <p class="sub-header">
                    {!! $jawabans->pertanyaan->pertanyaan !!}
                </p>
                <hr>
                <h4 class="header-title mt-2">Ubah Jawaban:</h4>
                <form action="{{ route('admin.jawabanUpdate') }}" method="post">
                    @csrf
                    @method('PUT')
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <textarea name="jawaban" id="summernote" required>{{ $jawabans->jawaban }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="correct">Benar/Salah:</label>
                                <select name="correct" id="correct" class="form-control" required>
                                    <option value="Y" {{ $jawabans->correct == 'Y' ? 'selected' : '' }}>Benar</option>
                                    <option value="N" {{ $jawabans->correct == 'N' ? 'selected' : '' }}>Salah</option>
                                </select>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="penilaian_id">Jenis Penilaian:</label>
                                <select name="penilaian_id" id="penilaian_id" class="form-control">
                                    @foreach($penilaians as $p)
                                        <option value="{{ $p->id }}" {{ $jawabans->penilaian->id == $p->id ? 'selected' : '' }}>{{ $p->nama_penilaian }} - Level {{ $p->level }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="hidden_id" value="{{ $jawabans->id }}">
                    <input type="hidden" name="pertanyaan_id" value="{{ $jawabans->pertanyaan_id }}">
                    <input type="submit" value="Update" class="btn btn-rounded btn-primary">
                    <a href="{{ route('admin.jawaban', $jawabans->pertanyaan_id) }}" class="btn btn-rounded btn-warning">Batal</a>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script>
    CKEDITOR.replace('summernote', {
        filebrowserImageBrowseUrl: '/filemanager?type=Images',
        filebrowserImageUploadUrl: '/filemanager/upload?type=Images&_token=',
        filebrowserBrowseUrl: '/filemanager?type=Files',
        filebrowserUploadUrl: '/filemanager/upload?type=Files&_token='
    });
</script>
@endsection