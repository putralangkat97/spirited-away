@extends('admin.layouts.master')

@section('title', 'Rekening')

@section('page-title', 'Rekening')

@section('content')
    <div class="row">
        <div class="col-xl-12">
            <div class="card-box table-responsive">
                <h4 class="header-title mb-2">Daftar Rekening</h4>
                <button type="button" class="sub-header btn btn-rounded btn-sm btn-primary text-white" id="tambah"><i class="fa fa-plus"></i></button>
                <table id="datatable" class="table table-bordered  dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead class="text-center">
                        <tr>
                            <th>#</th>
                            <th>Nama Bank</th>
                            <th>No. Rekening</th>
                            <th>A/N</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody class="text-center">
                        @foreach($banks as $key => $bank)
                        <tr>
                            <td>{{ ++$key }}</td>
                            <td>{{ $bank->nama_bank }}</td>
                            <td>{{ $bank->no_rekening }}</td>
                            <td>{{ $bank->nama_pemilik }}</td>
                            @if($bank->is_active == 0)
                            <td><span class="badge badge-danger badge-pill">Inactive</span></td>
                            @else
                            <td><span class="badge badge-success badge-pill">Active</span></td>
                            @endif
                            <td>
                                @if($bank->is_active == 0)
                                <a href="#" class="aktif btn btn-rounded btn-success" onclick="active({{ $bank->id }})" title="Aktifkan Akun"><i class="mdi mdi-power"></i></a>
                                @else
                                <a href="#" class="aktif btn btn-rounded btn-warning" onclick="inactive({{ $bank->id }})" title="Non aktifkan Akun"><i class="mdi mdi-power"></i></a>
                                @endif
                                &nbsp;
                                <a href="#" class="edit btn btn-rounded btn-info" title="Edit" id="{{ $bank->id }}"><i class="mdi mdi-pencil"></i></a>
                                &nbsp;
                                <a href="#" class="btn btn-rounded btn-danger" title="Hapus" onclick="deleteConfirmation({{ $bank->id }})"><i class="mdi mdi-trash-can"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@include('admin.modals._bank')
@endsection

@section('script')
<script src="{{ asset('assets/admin/js/bank.js') }}"></script>
@endsection