@extends('admin.layouts.master')

@section('title', 'Leaderboard')
    
@section('page-title', 'Leaderboard')

@section('content')
    @include('admin.modals._leaderboard')
    <div class="row">
        <div class="col-xl-12">
            <div class="card-box table-responsive">
                <h4 class="header-title mb-2">Leaderboard</h4>
                <table id="datatable" class="table table-bordered  dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead class="text-center">
                        <tr>
                            <th>#</th>
                            <th>Nama Peserta</th>
                            <th>Asal Sekolah</th>
                            <th>Jumlah Nilai</th>
                        </tr>
                    </thead>
                    <tbody class="text-center">
                        @foreach ($users as $key => $item)
                            @foreach ($item->users as $user)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $user->nama_lengkap }}</td>
                                <td>{{ $user->asal_sekolah }}</td>
                                <td>{{ $user->jumlah_point }}</td>
                            </tr>
                            @endforeach
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('assets/admin/js/leaderboard.js') }}"></script>
@endsection