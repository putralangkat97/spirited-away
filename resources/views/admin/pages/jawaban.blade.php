@extends('admin.layouts.master')

@section('title', 'Jawaban')

@section('page-title', 'Buat Jawaban')

@section('content')
    <div class="row">
        <div class="col-xl-12">
            <div class="card-box">
                @if(session()->has('message'))
                    <div class="alert alert-success text-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>Sukses! </strong>{{ session()->get('message') }}
                    </div>
                @elseif(session()->has('hapus'))
                    <div class="alert alert-danger text-danger alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>Sukses! </strong>{{ session()->get('hapus') }}
                    </div>
                @endif
                <h4 class="header-title mb-2">Pertanyaan:</h4>
                <p class="sub-header">
                    {!! $pertanyaans->pertanyaan !!}
                </p>
                <hr>
                <h4 class="header-title mt-2">Tulis Jawaban:</h4>
                <form action="{{ route('admin.jawabanAdd') }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <textarea name="jawaban" id="summernote" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="correct">Benar/Salah:</label>
                                <select name="correct" id="correct" class="form-control" required>
                                    <option value="">Pilih</option>
                                    <option value="Y">Benar</option>
                                    <option value="N">Salah</option>
                                </select>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="penilaian_id">Jenis Penilaian:</label>
                                <select name="penilaian_id" id="penilaian_id" class="form-control" required>
                                    <option value="">Pilih</option>
                                    @foreach($penilaians as $penilaian)
                                    <option value="{{ $penilaian->id }}">{{ $penilaian->nama_penilaian }} - Level {{ $penilaian->level }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="pertanyaan_id" value="{{ $pertanyaans->id }}">
                    <input type="submit" value="Submit" class="btn btn-rounded btn-primary">
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="card-box table-responsive">
                <h4 class="header-title">Daftar Jawaban</h4>
                <table id="datatable" class="table table-bordered  dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Jawaban</th>
                            <th>Correct</th>
                            <th>Jenis Penilaian</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($pertanyaans->jawabans as $key => $j)
                        <tr>
                            <td>{{ ++$key }}</td>
                            <td>{!! $j->jawaban !!}</td>
                            @if($j->correct == 'Y')
                            <td>Benar</td>
                            @else
                            <td>Salah</td>
                            @endif
                            <td>{{ $j->penilaian->nama_penilaian }}/Level: {{ $j->penilaian->level }}</td>
                            <td class="text-center">
                                <form action="{{ route('admin.jawabanDelete', $j->id) }}" method="POST">
                                    <a href="{{ route('admin.jawabanEdit', $j->id) }}" class="btn btn-rounded btn-info" title="Edit"><i class="mdi mdi-pencil"></i></a>
                                    &nbsp;
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-rounded btn-danger" title="Hapus"><i class="mdi mdi-trash-can"></i></button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
<script src="{{ asset('assets/admin/js/jawaban.js') }}"></script>
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script>
    CKEDITOR.replace('summernote', {
        filebrowserImageBrowseUrl: '/filemanager?type=Images',
        filebrowserImageUploadUrl: '/filemanager/upload?type=Images&_token=',
        filebrowserBrowseUrl: '/filemanager?type=Files',
        filebrowserUploadUrl: '/filemanager/upload?type=Files&_token='
    });
</script>
@endsection