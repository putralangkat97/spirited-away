<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Models\Province;
use App\Models\Kecamatan;
use App\Models\Olimpiade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function __construct() {
        $this->middleware('auth:admin');
    }

    public function userAdd() {
        $olimpiades = Olimpiade::orderBy('nama_olimpiade', 'asc')->get();
        // dd($a);
        $provinces = Province::pluck('name', 'id');
        return view('admin.pages.useradd', [
            'provinces' => $provinces, 'olimpiades' => $olimpiades
        ]);
    }

    public function userInsert(Request $request) {
        $data = array();
        $data['created_at'] = new \DateTime();
        $users = array(
            'nama_lengkap' => $request->nama_lengkap,
            'jenis_kelamin' => $request->jenis_kelamin,
            'asal_sekolah' => $request->asal_sekolah,
            'kelas' => $request->kelas,
            'provinsi_id' => $request->provinsi_id,
            'kabupaten_kota_id' => $request->kabupaten_kota_id,
            'username' => $request->username,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'tanggal_lahir' => date('Y-m-d', strtotime($request->tanggal_lahir)),
            'tempat_lahir' => $request->tempat_lahir,
            'alamat' => $request->alamat,
            'no_telepon_whatsapp' => $request->no_telepon_whatsapp,
            'is_active' => $request->is_active,
            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
        );

        $olimpiades = array(
            'olimpiade_id' => $request->olimpiade_id
        );

        $saveUser = User::create($users);
        $last_id = DB::getPDO()->lastInsertId();

        foreach ($olimpiades as $key) {
            foreach ($key as $value) {
                $olimusers = DB::table('olimpiade_user')->insert([
                    'olimpiade_id'  => $value,
                    'user_id'       => $last_id
                ]);
            }
        }

        return redirect()->route('admin.user')
            ->with('success', $request->nama_lengkap.' berhasil ditambahkan');
    }

    public function kota(Request $request) {
        $cities = Kecamatan::where('province_id', $request->get('id'))
            ->pluck('name', 'id');

        return response()->json($cities);
    }

    public function user() {
        $users = User::orderBy('id', 'desc')->get();
        return view('admin.pages.user')
            ->with(['users' => $users]);
    }

    public function userActive($id) {
        $data = User::where('id', $id)->update([
            'is_active' => 1,
        ]);
        // check data deleted or not
        if ($data == 1) {
            $success = true;
            $message = "Data activated successfully";
        } else {
            $success = true;
            $message = "Data not found";
        }
        //  Return response
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }

    public function userInactive($id) {
        $data = User::where('id', $id)->update([
            'is_active' => 0,
        ]);
        // check data deleted or not
        if ($data == 1) {
            $success = true;
            $message = "Data activated successfully";
        } else {
            $success = true;
            $message = "Data not found";
        }
        //  Return response
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }

    public function userDelete($id) {
        $tes = DB::table('olimpiade_user')->where('user_id', '=', $id)
            ->delete();
        $data = User::where('id', $id)->delete();

        // check data deleted or not
        if ($data == 1) {
            $success = true;
            $message = "Data activated successfully";
        } else {
            $success = true;
            $message = "Data not found";
        }
        //  Return response
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }

    public function userDetail($id) {
        $users = User::where('id', $id)->with('province')
            ->with('kecamatan')
            ->get();

        foreach ($users as $user) {
            $user;
        }

        // dd($tes);

        return response()->json(['user' => $user]);
    }
}
