<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLog extends Model
{
    protected $guarded = [];

    public function olimpiade() {
        return $this->belongsTo('App\Models\Olimpiade', 'olimpiade_id');
    }

    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }
}
