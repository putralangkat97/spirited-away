<div id="modalAdmin" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Admin</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form id="formAdmin" method="post" data-parsley-validate>
                @csrf
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="nama_admin">Nama Admin:</label>
                                <input type="text" name="nama_admin" id="nama_admin" class="form-control" required placeholder="Nama Admin" data-parsley-required-message="Nama tidak boleh kosong.">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="email">E-mail:</label>
                                <input type="text" name="email" id="email" class="form-control" required placeholder="E-Mail" data-parsley-required-message="Email tidak boleh kosong.">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="password">Password:</label>
                                <input type="password" name="password" id="password" class="form-control" required placeholder="Password" data-parsley-required-message="Password tidak boleh kosong.">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="confirm-password">Konfirmasi Password:</label>
                                <input type="password" name="confirm-password" id="confirm-password" onchange="check();" class="form-control" required placeholder="Password" data-parsley-required-message="Konfirmasi password terlebih dahulu.">
                                <span id="tes"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <input type="hidden" name="hidden_id" id="id">
                            <input type="hidden" name="add" id="tambah" value="Tambah">
                            <input class="btn btn-primary btn-block" type="submit" id="action" value="tambah">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>