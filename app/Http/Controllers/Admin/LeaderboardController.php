<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Models\Result;
use App\Models\Olimpiade;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LeaderboardController extends Controller
{
    public function __construct() {
        $this->middleware('auth:admin');
    }

    public function leaderboard() {
        $events = Olimpiade::orderby('nama_olimpiade', 'asc')->get();
        $users  = User::where('is_active', 1)
            ->orderBy('jumlah_point', 'desc')
            ->with([
                'resultUsers' => function ($q) {
                    $q->select('user_id', 'logged_in')
                        ->groupBy('user_id');
                }
            ])
            ->whereHas('resultUsers')
            ->get();

        return view('admin.pages.leaderboard', [
            'events' => $events, 'users' => $users
        ]);
    }

    public function leaderboardResult(Request $request) {
        $events = Olimpiade::orderby('nama_olimpiade', 'asc')->get();
        $users  = Olimpiade::where('id', $request->event)
            ->with([
                'users' => function ($query) {
                    $query->where('is_active', 1)->whereHas('olimpiades')->get();
                }
            ])->get();
        
        return view('admin.pages.leaderboardResult')
            ->with([
                'users' => $users, 'events' => $events
            ]);
    }
}
