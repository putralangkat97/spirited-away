<div class="left-side-menu">
    <div class="slimscroll-menu">
        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <ul class="metismenu" id="side-menu">
                <li class="menu-title">Navigasi Menu</li>
                
                <li>
                    <a href="{{ route('admin.home') }}">
                        <i class="fe-airplay"></i>
                        <span> Dashboard </span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('admin.leaderboard') }}">
                        <i class="mdi mdi-sword-cross"></i>
                        <span> Leaderboard </span>
                    </a>
                </li>

                <li>
                    <a href="javascript: void(0);">
                        <i class="fe-sidebar"></i>
                        <span>  Modul </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="{{ route('admin.topik') }}"><i class="mdi mdi-content-paste "></i> Event-Bidang</a></li>
                        <li><a href="{{ route('admin.quiz') }}"><i class="mdi mdi-file-document-box-outline"></i> Quiz</a></li>
                        <li><a href="#"><i class="mdi mdi-check-underline"></i> Result</a></li>
                    </ul>
                </li>

                <li>
                    <a href="{{ route('admin.penilaian') }}">
                        <i class="mdi mdi-chart-timeline"></i>
                        <span> Penilaian </span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('admin.user') }}">
                        <i class="icon-people"></i>
                        <span> User </span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('admin.pembayaran') }}">
                        <i class="mdi mdi-cash-multiple"></i>
                        <span> Pembayaran </span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('admin.bank') }}">
                        <i class="mdi mdi-bank"></i>
                        <span> Rekening </span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('admin.sertifikat') }}">
                        <i class="mdi mdi-file-document"></i>
                        <span> Sertifikat </span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('admin.tambahAdmin') }}">
                        <i class="mdi mdi-account"></i>
                        <span> Admin </span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- End Sidebar -->
        <div class="clearfix"></div>
    </div>
    <!-- Sidebar -left -->
</div>