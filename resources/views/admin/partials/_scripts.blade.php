        <script src="{{ asset('assets/js/vendor.min.js') }}"></script>
        <!--C3 Chart-->
        <script src="{{ asset('assets/libs/d3/d3.min.js') }}"></script>
        <script src="{{ asset('assets/libs/c3/c3.min.js') }}"></script>
        <script src="{{ asset('assets/js/pages/c3.init.js') }}"></script>
        <script src="{{ asset('assets/libs/echarts/echarts.min.js') }}"></script>
        <script src="{{ asset('assets/js/pages/dashboard.init.js') }}"></script>
        <!-- Required datatable js -->
        <script src="{{ asset('assets/libs/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('assets/libs/datatables/dataTables.bootstrap4.min.js') }}"></script>
        <!-- Responsive examples -->
        <script src="{{ asset('assets/libs/datatables/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('assets/libs/datatables/responsive.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('js/gijgo.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap-datetimepickers.min.js') }}"></script>
        {{-- <script src="{{ asset('js/bootstrap-datepicker.id.min.js') }}"></script> --}}
        <script src="{{ asset('js/sweetalert2.all.min.js') }}"></script>
        <script src="{{ asset('js/parsley.js') }}"></script>
        <script src="{{ asset('js/jquery.validate.min.js') }}"></script>
        <!-- App js -->
        <script src="{{ asset('assets/js/app.min.js') }}"></script>