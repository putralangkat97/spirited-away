<div id="modalQuiz" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Topik</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form id="formQuiz" method="post" data-parsley-validate>
                @csrf
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="olimpiade_id">Pilih Kategori:</label>
                                <select name="olimpiade_id" id="olimpiade_id" class="form-control" required>
                                    <option value="">Pilih</option>
                                    @foreach($olimpiades as $olimpiade)
                                    <option value="{{ $olimpiade->id }}">{{ $olimpiade->nama_olimpiade }} {{ $olimpiade->tingkat }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="waktu">Waktu:</label>
                                <div class="input-group">
                                    <input type="text" name="waktu" id="waktu" class="form-control" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" required placeholder="0" data-parsley-required-message="Waktu tidak boleh kosong.">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">menit</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="kode_soal">Kode soal:</label>
                                <input type="text" id="randomString" class="form-control" name="kode_soal" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <input type="hidden" name="hidden_id" id="id">
                            <input type="hidden" name="add" id="tambah" value="Tambah">
                            <input class="btn btn-primary btn-block" type="submit" id="action" value="tambah">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>