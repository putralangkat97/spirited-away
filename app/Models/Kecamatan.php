<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kecamatan extends Model
{
    protected $guarded = [];
    protected $table = 'indonesia_cities';

    public function users() {
        return $this->hasMany('App\User', 'kabupaten_kota_id');
    }

    public function provinsi() {
        return $this->belongsTo('App\Models\Province', 'province_id');
    }
}
