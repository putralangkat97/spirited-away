<!DOCTYPE html>
<html lang="en">
<head>
    <title>POSI | {{ Auth::user()->username }}</title>
    @include('user.partials._head')
</head>

<style>
    .uk-navbar-container:not(.uk-navbar-transparent) {
        background: #3567c8;
    }

    .top-right a {
        color: #FFF;
    }

    .container {
        margin: 8rem auto;
        max-width: 1280px;
    }
</style>

@yield('style')

<body>
    @include('user.partials._navbar')

    @yield('content')

    @include('user.partials._script')
    @yield('script')
</body>
</html>
