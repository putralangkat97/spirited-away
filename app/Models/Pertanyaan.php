<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    protected $guarded = [];

    public function quiz() {
        return $this->belongsTo('App\Models\Quiz', 'quiz_id');
    }

    public function jawabans() {
        return $this->hasMany('App\Models\Jawaban', 'pertanyaan_id');
    }

    public function resultPertanyaans() {
        return $this->belongsToMany('App\Models\Result', 'pertanyaan_id');
    }
}
