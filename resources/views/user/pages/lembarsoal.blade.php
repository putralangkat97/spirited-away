<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>POSI | {{ Auth::user()->username }}</title>
    @include('user.partials._head')
    <style>
        html {
            scroll-behavior: smooth;
        }

        .waktunya {
            text-align: right;
        }

        .navbar-custom {
            background-color: #3567c8;
        }

        [type="radio"]:checked,
        [type="radio"]:not(:checked) {
            position: absolute;
            left: -9999px;
        }
    
        [type="radio"]:checked + label,
        [type="radio"]:not(:checked) + label
        {
            position: relative;
            padding-left: 28px;
            cursor: pointer;
            line-height: 20px;
            display: inline-block;
            color: #666;
        }
    
        [type="radio"]:checked + label:before,
        [type="radio"]:not(:checked) + label:before {
            content: '';
            position: absolute;
            left: 0;
            top: 0;
            width: 20px;
            height: 20px;
            border: 1px solid #ddd;
            border-radius: 100%;
            background: #fff;
        }
    
        [type="radio"]:checked + label:after,
        [type="radio"]:not(:checked) + label:after {
            content: '';
            width: 12px;
            height: 12px;
            background: #6add66;
            position: absolute;
            top: 4px;
            left: 4px;
            border-radius: 100%;
            -webkit-transition: all 0.2s ease;
            transition: all 0.2s ease;
        }
    
        [type="radio"]:not(:checked) + label:after {
            opacity: 0;
            -webkit-transform: scale(0);
            transform: scale(0);
        }
    
        [type="radio"]:checked + label:after {
            opacity: 1;
            -webkit-transform: scale(1);
            transform: scale(1);
        }
    
        p {
            margin-top: 0;
            margin-bottom: 0;
        }
    
        .tes {
            margin-top: 8rem;
        }
        #sidebar-menu > ul > li > a {
            color: #95a4b5;
            display: inline-block;
            padding: 11px 20px;
            position: relative;
            margin: 2px 0;
                margin-top: 2px;
                margin-right: 0px;
            -webkit-transition: all .4s;
            transition: all .4s;
            font-size: 14.4px;
        }
    </style>
</head>
<body>
    <!-- Begin page -->
    <div id="wrapper">
        <!-- Topbar Start -->
        <div class="navbar-custom">
            <ul class="list-unstyled topnav-menu float-right mb-0">
                <li class="dropdown notification-list">
                    <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                        <img src="{{ asset('img/male.png') }}" alt="user-image" class="rounded-circle">
                        <span class="pro-user-name ml-1">
                            {{ Auth::user()->username }} 
                        </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                        <!-- item-->
                        {{-- <div class="dropdown-header noti-title">
                            <h6 class="text-overflow m-0">Kembali</h6>
                        </div> --}}
                        <!-- item-->
                        <div class="dropdown-divider"></div>
                        <a href="{{ url('/') }}" class="dropdown-item notify-item">
                            <i class="mdi mdi-home"></i> Home
                        </a>
                        <!-- item-->
                        <a href="{{ route('user.logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="dropdown-item notify-item">
                            <i class="fe-log-out"></i>
                            <span>{{ __('Logout') }}</span>
                        </a>
                        <form id="logout-form" action="{{ route('user.logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
        
                    </div>
                </li>
            </ul>
        
            <!-- LOGO -->
            <div class="logo-box">
                <a href="#" class="logo text-center">
                    <span class="logo-lg">
                        <img src="{{ asset('img/posi.png') }}" alt="" width="200">
                        <!-- <span class="logo-lg-text-light">UBold</span> -->
                    </span>
                    <span class="logo-sm">
                        <!-- <span class="logo-sm-text-dark">U</span> -->
                        <img src="{{ asset('img/posi.png') }}" alt="" height="15">
                    </span>
                </a>
            </div>
        
            <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
                <li>
                    <button class="button-menu-mobile waves-effect waves-light">
                        <i class="fe-menu"></i>
                    </button>
                </li>
            </ul>
        </div>
        <!-- end Topbar -->
        
        <!-- ========== Left Sidebar Start ========== -->
        <div class="left-side-menu">
            <div class="slimscroll-menu">
                <!--- Sidemenu -->
                <div id="sidebar-menu">
                    <ul class="metismenu" id="side-menu">
                        {{-- <li class="menu-title">Time left:
                            <span id="time"></span>
                        </li> --}}
                        <li class="menu-title"><span>Navigasi Soal:</span></li>
                        <li class="ml-3 mr-2">
                        @foreach ($soals as $soal)
                            @foreach ($soal->pertanyaans as $item => $o)
                                <a href="#{{ $o->id }}" class="btn btn-sm btn-outline-success btn-rounded mr-1 mt-2 page-scroll text-white">{{ ++$item }}</a>
                            @endforeach
                        @endforeach
                        </li>
                        <hr>
                        <li class="ml-3 mr-2">
                            <a href="#selesai" class="btn btn-info btn-lg btn-rounded page-scroll text-white">Selesai</a>
                        </li>
                    </ul>
                </div>
                <!-- End Sidebar -->
                <div class="clearfix"></div>
            </div>
            <!-- Sidebar -left -->
        </div>
        <!-- Left Sidebar End -->

        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <div class="content"> 
                <!-- Start Content-->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-6">
                            <div class="page-title-box">
                                <h4 class="page-title">{{ $soal->olimpiade->nama_olimpiade }} {{ $soal->olimpiade->tingkat }} <small>({{ $soal->kode_soal }})</small></h4>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="page-title-box waktunya">
                                <h4 class="page-title">Sisa Waktu: <span id="time"></span></h4>
                            </div>
                        </div>
                    </div>  
                    <!-- start row -->
                    <div class="row">
                        <div class="col-xl-12">
                            <form action="{{ route('user.storeTest') }}" method="post" id="kirim">
                            @csrf
                            <input type="hidden" name="waktu" value="{{ \Carbon\Carbon::now('Asia/Jakarta') }}">
                            @foreach ($soals as $soal)
                                <input type="hidden" name="olimpiade_id" value={{ $soal->olimpiade_id }}>
                                <div class="card-box">
                                    {{-- <h5 class="d-flex justify-content-between align-items-center mb-3"></h5> --}}
                                    @foreach ($soal->pertanyaans as $nn => $p)
                                    <div class="card @if(!$loop->last)mb-3 @endif" id="{{ $p->id }}">
                                        <div class="card-header">
                                            <h6>No. {{ ++$nn }}</h6>
                                            <h4>{!! $p->pertanyaan !!}</h4>
                                            {{-- <input type="hidden" name="pertanyaan[]" value=""> --}}
                                        </div>
                                        <div class="card-body">
                                            @foreach ($p->jawabans as $j)
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="answer[{{ $p->id }}]" id="option-{{ $j->id }}" value="{{ $j->id }}">
                                                <label class="form-check-label" for="option-{{ $j->id }}">
                                                    {!! $j->jawaban !!}
                                                </label>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    @endforeach
                                    <div class="card-body">
                                        <button id="selesai" type="submit" class="btn btn-info">Selesai</button>
                                    </div>
                                </div>
                            @endforeach
                            </form>
                        </div>
                    </div>
                    <!--- end row -->
                </div> <!-- end container-fluid -->
            </div> <!-- end content -->

            <!-- Footer Start -->
            @include('user.partials._footer')
            <!-- end Footer -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page content -->
        <!-- ============================================================== -->
    </div>
    <!-- END wrapper -->

    <!-- Javascript/Add-ons scripts -->
    @include('user.partials._script')
    <script>
        $(document).ready(function () {
            $('a').on('click', function (event) {
                if (this.hash !== "") {
                // Prevent default anchor click behavior
                    event.preventDefault();
                    // Store hash
                    var hash = this.hash;
                    // Using jQuery's animate() method to add smooth page scroll
                    // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
                    $('html, body').animate({
                        scrollDown: $(hash).offset().top - 800
                    }, 200, 'easeOutBounce', function(){
                        // Add hash (#) to URL when done scrolling (default click behavior)
                        window.location.hash = hash;
                    });
                } // End if
            });
        });
    </script>
    <script>

        function startTimer(duration, display) {
            var timer = duration, minutes, seconds;
            let idTimer = setInterval(function () {
                hours = Math.floor(timer / 60 / 60);
                minutes = Math.floor(timer / 60) % 60;
                seconds = timer % 60;
                hours       = hours < 10 ? "0" + hours : hours;
                minutes     = minutes < 10 ? "0" + minutes : minutes;
                seconds     = seconds < 10 ? "0" + seconds : seconds;
                display.textContent = hours + ":" + minutes + ":" + seconds;
                if (--timer < 0) {
                    clearInterval(idTimer);
                    SubmitFunction();
                }
            }, 1000);
        }

        function SubmitFunction() {
            time.innerHTML="Waktu Habis!";
            document.getElementById('kirim').submit();
        }

        var menit = {!! $waktu[0] !!};
        window.onload = function () {
            var fiveMinutes = 60 * menit,
                display = document.querySelector('#time');
            startTimer(fiveMinutes, display);
        };
    </script>
</body>
</html>