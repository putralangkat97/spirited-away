@extends('admin.layouts.master')

@section('title', 'Quiz')

@section('page-title', 'Quiz')

@section('content')
    <div class="row">
        <div class="col-xl-12">
            <div class="card-box table-responsive">
                <h4 class="header-title mb-2">Quiz</h4>
                <button type="button" class="sub-header btn btn-rounded btn-sm btn-primary text-white" id="tambah"><i class="fa fa-plus"></i></button>
                <table id="datatable" class="table table-bordered  dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead class="text-center">
                        <tr>
                            <th>#</th>
                            <th>ID</th>
                            <th>Kategori</th>
                            <th>Kode Soal</th>
                            <th>Waktu</th>
                            <th>Jumlah Pertanyaan</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody class="text-center">
                        @php
                            $no = 0;
                        @endphp
                        @foreach($quizzes as $q)
                        @php
                            $no++;
                        @endphp
                        @if($q->olimpiade)
                        <tr>
                            <td>{{ $no }}</td>
                            <td>{{ $q->id }}</td>
                            <td>
                                {{ $q->olimpiade->nama_olimpiade }} {{ $q->olimpiade->tingkat }}
                            </td>
                            <td>{{ $q->kode_soal }}</td>
                            <td>{{ $q->waktu }} menit</td>
                            <td>{{ $q->pertanyaans->count() }}</td>
                            <td>
                                <a href="{{ route('admin.pertanyaan', $q->id) }}" class="btn btn-warning btn-rounded" title="Buat Pertanyaan"><i class="mdi mdi-pencil-plus"></i></a>
                                &nbsp;
                                <a href="#" class="edit btn btn-rounded btn-info" id="{{ $q->id }}" title="Edit"><i class="mdi mdi-pencil"></i></a>
                                &nbsp;
                                <a href="#" class="hapus btn btn-rounded btn-danger" onclick="deleteConfirmation({{ $q->id }})" title="Hapus"><i class="mdi mdi-trash-can"></i></a>
                            </td>
                            @else
                            <td>{{ $no }}</td>
                            <td>
                                <span class="text-danger">Kategori telah dihapus</span>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@include('admin.modals._quiz')
@endsection

@section('script')
<script src="{{ asset('assets/admin/js/quiz.js') }}"></script>
@endsection