<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOlimpiadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('olimpiades', function (Blueprint $table) {
            $table->id();
            $table->string('nama_olimpiade', 100);
            $table->datetime('start_date');
            $table->datetime('end_date');
            $table->boolean('is_active')->default(0);
            $table->text('deskripsi');
            $table->string('tingkat', 20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('olimpiades');
    }
}
