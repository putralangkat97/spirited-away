<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pembayaran extends Model
{
    protected $guarded = [];

    public function bank() {
        return $this->belongsTo('App\Models\Bank', 'bank_id');
    }

    public function user() {
    return $this->belongsTo('App\User', 'user_id');
    }
}
