@extends('user.layouts.master')

@section('content')
    <div class="row">
        <div class="container">
            <div class="col-xl-12">
                <div class="card-box">
                    @if ($success)
                    <div class="card-body">
                        <div class="alert alert-success">
                            <strong>{{ $success }}</strong>
                        </div>
                        <h4>
                            <p class="lead">
                                <span>Skor kamu: <strong>{{ $point }}</strong></span>
                            </p>
                            {{-- <p class="lead">
                                <span>Benar: <strong class="text-success"></strong></span><br>
                                <span>Salah: <strong class="text-danger"></strong></span>
                            </p> --}}
                        </h4>
                        <h4>
                            <p class="lead">Silahkan kembali kehalaman utama</p>
                            <p><a href="{{ url('/dashboard') }}" class="btn btn-primary">Kembali</a></p>
                        </h4>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection