$(document).ready(function () {
    $('#datatable').DataTable()

    $('#tambah').click( function () {
        $('.modal-title').text('Tambah Informasi Rekening')
        $('#tambah').val('Tambah')
        $('#action').val('Tambah')
        $('#modalBank').modal('show')
    })

    $('#formBank').on('submit', function (event) {
        event.preventDefault()
        var id = $(this).attr('id')
        var url = ''

        if ($('#tambah').val() == 'Tambah') {
            url = '/admin/bank/add'
            text = 'Rekening berhasil ditambahkan'
        }

        if ($('#tambah').val() == 'Edit') {
            url = '/admin/bank/update'
            text = 'Rekening behasil diupdate'
        }

        $.ajax({
            url: url,
            data: $(this).serialize(),
            dataType: 'JSON',
            method: 'POST',
            success: function (data) {
                if (data.success) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Sukses',
                        text: text
                    }).then(function () {
                        location.reload()
                    })
                    $('#formBank').modal('hide')
                    $('#formBank')[0].reset()
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Gagal',
                        text: 'Terdapat kesalahan saat memasukkan data'
                    }).then(function () {
                        $('#formBank')[0].reset()
                        $('#modalBank').modal('hide')
                    })
                }
            }
        })
    })

    $(document).on('click', '.edit', function () {
        var id = $(this).attr('id')
        $.ajax({
            url: '/admin/bank/edit/'+id,
            dataType: 'JSON',
            success: function (data) {
                $('#id').val(data.bank.id)
                $('#nama_pemilik').val(data.bank.nama_pemilik)
                $('#no_rekening').val(data.bank.no_rekening)
                $('#nama_bank').val(data.bank.nama_bank)
                $('.modal-title').text('Edit Informasi Rekening')
                $('#tambah').val('Edit')
                $('#action').val('Update')
                $('#modalBank').modal('show')
            }
        })
    })
})

function inactive(id) {
    Swal.fire({
        icon: 'warning',
        title: "Yang bener..?",
        text: "Anda yakin ingin menon-aktifkan akun ini?",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Matikan",
        cancelButtonText: "Batal",
        reverseButtons: !0
    }).then(function (event) {

        if (event.value === true) {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                type: 'POST',
                url: '/admin/bank/inactive/'+id,
                data: { _token: CSRF_TOKEN },
                dataType: 'JSON',
                success: function (results) {
                    if (results.success === true) {
                        Swal.fire({
                            // toast: true,
                            icon: 'success',
                            title: "Success",
                            text: "Akun Sudah Tidak Aktif",
                            showConfirmButton: true,
                            timer: 1500
                        }).then(function() {
                            location.reload();
                        });
                        $('#sample_form')[0].reset();
                    } else {
                        Swal.fire("Error!", results.message, "error");
                    }
                }
            });

        } else {
            e.dismiss;
        }

    }, function (dismiss) {
        return false;
    })
}

function active(id) {
    Swal.fire({
        icon: 'warning',
        title: "Yang bener..?",
        text: "Anda yakin ingin mengaktifkan akun ini?",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Aktifkan",
        cancelButtonText: "Batal",
        reverseButtons: !0
    }).then(function (event) {

        if (event.value === true) {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                type: 'POST',
                url: '/admin/bank/active/'+id,
                data: { _token: CSRF_TOKEN },
                dataType: 'JSON',
                success: function (results) {
                    if (results.success === true) {
                        Swal.fire({
                            // toast: true,
                            icon: 'success',
                            title: "Success",
                            text: "Akun Aktif",
                            showConfirmButton: true,
                            timer: 1500
                        }).then(function() {
                            location.reload();
                        });
                        $('#sample_form')[0].reset();
                    } else {
                        Swal.fire("Error!", results.message, "error");
                    }
                }
            });

        } else {
            e.dismiss;
        }

    }, function (dismiss) {
        return false;
    })
}

function deleteConfirmation(id) {
    Swal.fire({
        icon: 'warning',
        title: "Yang bener..?",
        text: "Anda yakin ingin menghapus data ini?",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Hapus",
        cancelButtonText: "Batal",
        reverseButtons: !0
    }).then(function (event) {

        if (event.value === true) {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                type: 'POST',
                url: '/admin/bank/delete/'+id,
                data: { _token: CSRF_TOKEN },
                dataType: 'JSON',
                success: function (results) {
                    if (results.success === true) {
                        Swal.fire({
                            // toast: true,
                            icon: 'success',
                            title: "Success",
                            text: "Data Deleted",
                            showConfirmButton: true,
                            timer: 1500
                        }).then(function() {
                            location.reload();
                        });
                        $('#sample_form')[0].reset();
                    } else {
                        Swal.fire("Error!", results.message, "error");
                    }
                }
            });

        } else {
            e.dismiss;
        }

    }, function (dismiss) {
        return false;
    })
}