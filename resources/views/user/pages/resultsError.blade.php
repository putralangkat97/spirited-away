@extends('user.layouts.master')

@section('content')
    <div class="row">
        <div class="container">
            <div class="col-xl-12">
                <div class="card-box">
                    @if($error)
                    <div class="card-body">
                        <div class="alert alert-danger">
                            <strong>{{ $error }}</strong>
                        </div>
                        <h4>
                            <p class="lead">Silahkan kembali kehalaman utama</p>
                            <p><a href="{{ url('/dashboard') }}" class="btn btn-primary">Kembali</a></p>
                        </h4>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection