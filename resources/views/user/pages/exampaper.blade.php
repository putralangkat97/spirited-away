@extends('user.layouts.master')

@section('content')
    <div class="container">
        <div class="col-xl-12">
            <div class="card-box">
                @if (Auth::user()->is_active == 0)
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3 order-md-3 mb4">
                                <div class="alert alert-danger">
                                    <strong>Akun belum aktif</strong>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3 order-md-2 mb-4">
                            <h4 class="d-flex justify-content-between align-items-center mb-3">
                                <span>Keterangan:</span>
                            </h4>
                            @foreach ($quizzes as $quiz)
                            <ul class="list-group mb-3">
                                <li class="d-flex justify-content-between lh-condensed">
                                    <div>
                                    <span class="my-0">Kategori:</span>
                                    </div>
                                    <span><strong>{{ $quiz->olimpiade->nama_olimpiade }}</strong></span>
                                </li>
                                <li class="d-flex justify-content-between lh-condensed">
                                    <div>
                                    <span class="my-0">Tingkat:</span>
                                    </div>
                                    <span><strong>{{ $quiz->olimpiade->tingkat }}</strong></span>
                                </li>
                                <li class="d-flex justify-content-between lh-condensed">
                                    <div>
                                    <span class="my-0">Durasi:</span>
                                    </div>
                                    <span><strong>{{ $quiz->waktu }} menit</strong></span>
                                </li>
                                <li class="d-flex justify-content-between lh-condensed">
                                    <div>
                                    <span class="my-0">Kode Soal:</span>
                                    </div>
                                    <span><strong>{{ $quiz->kode_soal }}</strong></span>
                                </li>
                                <li class="d-flex justify-content-between lh-condensed">
                                    <div>
                                    <span class="my-0">Jenis Soal:</span>
                                    </div>
                                    <span><strong>Pilihan Berganda</strong></span>
                                </li>
                                <li class="d-flex justify-content-between lh-condensed">
                                    <div>
                                    <span class="my-0">Sifat:</span>
                                    </div>
                                    <span><strong>Close Book</strong></span>
                                </li>
                            </ul>
                            @endforeach
                        </div>
                        <div class="col-md-9 order-md-1">
                            <h4 class="mb-3">Deskripsi</h4>
                            @foreach ($quizzes as $item)
                            <p class="lead">{{ $item->olimpiade->deskripsi }}</p>
                            <a href="{{ route('user.lembarsoal', $item->kode_soal) }}" class="btn btn-lg btn-rounded btn-primary">Lihat Lembar Soal</a>
                            @endforeach
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
@endsection