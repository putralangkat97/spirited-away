<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Penilaian extends Model
{
    protected $guarded = [];

    public function jawabans() {
        return $this->hasMany('App\Models\Jawaban');
    }
}
