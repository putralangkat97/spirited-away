<?php

namespace App\Http\Controllers\Admin;

use App\Models\Olimpiade;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EventController extends Controller
{
    // contruct
    public function __construct() {
        $this->middleware('auth:admin');
    }
    
    // Topik //
    public function topik() {
        $olimpiades = Olimpiade::orderBy('id', 'desc')->get();
        return view('admin.pages.topik')->with('olimpiades', $olimpiades);
    }

    public function topikAdd(Request $request) {
        $form = array(
            'nama_olimpiade'    => $request->nama_olimpiade,
            'tingkat'           => $request->tingkat,
            'start_date'        => date('Y-m-d H:i:s', strtotime($request->start_date)),
            'end_date'          => date('Y-m-d H:i:s', strtotime($request->end_date)),
            'deskripsi'         => $request->deskripsi
        );

        Olimpiade::create($form);

        return response()->json(['success' => 'Topik berhasil ditambahkan']);
    }

    public function topikEdit($id) {
        $olimpiades = Olimpiade::findOrFail($id);

        return response()->json(['topik' => $olimpiades]);
    }

    public function topikUpdate(Request $request, Olimpiade $olimpiade) {
        $form = array(
            'nama_olimpiade'    => $request->nama_olimpiade,
            'tingkat'           => $request->tingkat,
            'start_date'        => date('Y-m-d H:i:s', strtotime($request->start_date)),
            'end_date'          => date('Y-m-d H:i:s', strtotime($request->end_date)),
            'deskripsi'         => $request->deskripsi
        );

        Olimpiade::whereId($request->hidden_id)->update($form);

        return response()->json(['success' => 'Topik berhasil diupdate']);
    }

    public function topikDelete($id) {
        $data = Olimpiade::where('id', $id)->delete();
        // check data deleted or not
        if ($data == 1) {
            $success = true;
            $message = "Data deleted successfully";
        } else {
            $success = true;
            $message = "Data not found";
        }
        //  Return response
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }

    public function aktifkan($id) {
        $data = Olimpiade::where('id', $id)
            ->update(['is_active' => 1]);
        
        // check data deleted or not
        if ($data == 1) {
            $success = true;
            $message = "Data activated successfully";
        } else {
            $success = true;
            $message = "Data not found";
        }

        //  Return response
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }

    public function nonaktifkan($id) {
        $data = Olimpiade::where('id', $id)
            ->update(['is_active' => 0]);
        
        // check data deleted or not
        if ($data == 1) {
            $success = true;
            $message = "Data deactivated successfully";
        } else {
            $success = true;
            $message = "Data not found";
        }

        //  Return response
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }
}
