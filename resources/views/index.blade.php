@extends('layouts.index')

@section('content')
<header class="uk-cover-container uk-background-cover uk-background-norepeat uk-background-center-center"
style="background-image: url(img/working.jpg);">
    <video src="img/working.mp4" data-uk-cover></video>
    <div class="uk-overlay uk-position-cover uk-overlay-video"></div>
    <div data-uk-sticky="animation: uk-animation-slide-top; sel-target: .uk-navbar-container;
    cls-active: uk-navbar-sticky; cls-inactive: uk-navbar-transparent uk-light; top: 500">
    <br>
        <nav class="uk-navbar-container uk-letter-spacing-small uk-text-bold">
            <div class="uk-container uk-container-large">
                <div class="uk-position-z-index" data-uk-navbar>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-3 col-sm-3">
                                <a class="uk-navbar-item uk-logo" href="/"><img src="{{ asset('img/posi.png') }}" style="max-width: 230px;"></a>
                            </div>
                            <div class="col-lg-5 col-sm-6"></div>
                            <div class="col-lg-4 col-sm-3">
                                <div class="uk-navbar-item">
                                    @if (Route::has('login'))
                                        <div class="top-right links">
                                            @auth
                                                <a href="/dashboard">
                                                    @if(Auth::user()->jenis_kelamin == 'Laki-Laki')
                                                        <img src="{{ asset('img/male.png') }}" alt="User image" width="40">
                                                    @elseif(Auth::user()->jenis_kelamin == 'Perempuan')
                                                        <img src="{{ asset('img/female.png') }}" alt="User image" width="40">
                                                    @endif
                                                    {{ Auth::user()->username }}
                                                </a>
                                                |
                                                <a href="{{ route('user.logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                {{ __('Logout') }}
                                                </a>

                                                <form id="logout-form" action="{{ route('user.logout') }}" method="POST" style="display: none;">
                                                @csrf
                                                </form>
                                    @else
                                            <div><a class="uk-button uk-button-success-outline" href="/login">Sign In</a></div>
                                            @endauth
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </div>
    <form action="{{ route('user.exampaper') }}" id="cari" method="get">
    @csrf
        <div class="uk-container uk-container-large uk-light" data-uk-height-viewport="offset-top: true">
            <div data-uk-grid data-uk-height-viewport="offset-top: true">
                <div class="col-lg-2"></div>
                <div class="uk-width-expand@m uk-section uk-flex uk-flex-column" style="margin-top: -100px;">
                    <div class="uk-margin-auto-top uk-margin-auto-bottom" align="left">
                        <div class="uk-grid-collapse uk-width-3-4@m uk-margin-medium-top tes" data-uk-grid
                        data-uk-scrollspy="cls: uk-animation-slide-bottom-medium; delay: 200; repeat: true">

                            <div class="uk-width-expand">
                                <input name="cari" class="uk-input uk-form-large uk-border-remove-right" type="text" placeholder="Temukan Soal">
                            </div>
                            <div class="uk-width-auto">
                                <button type="submit" class="uk-button uk-button-large uk-button-success-outline">Cari</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2"></div>
            </div>

            <div class="row">
                <div class="col-lg-12" style="text-align: center;">
                    <p>Pelatihan Olimpiade Sains Indonesia © 2020 <font color="white">Design by  <a href="https//n56ht.com">Insight MarComm</font></a></p>
                </div>
            </div>
        </div>
    </form>
</header>
@endsection

@section('script')
<script>
    $(document).ready(function () {
        console.log('Halo')
    })
</script>
@endsection
