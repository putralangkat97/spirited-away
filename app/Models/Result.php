<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    protected $guarded = [];

    public function userResult() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function pertanyaanResults() {
        return $this->belongsToMany('App\Models\Pertanyaan', 'pertanyaan_id');
    }

    public function jawabanResults() {
        return $this->belongsToMany('App\Models\Jawaban', 'jawaban_id');
    }

    public function olimpiadeResult() {
        return $this->belongsTo('App\Models\Olimpiade', 'olimpiade_id');
    }
}
