@extends('admin.layouts.master')

@section('title', 'User Import')

@section('page-title', 'Import User')

@section('content')
    <div class="row">
        <div class="col-xl-12">
            <div class="card-box">
				<h4 class="header-title mb-2">Mohon baca terlebih dahulu petunjuk pengisian data user sebelum melakukan import data:</h4>
				@if (session()->has('error'))
					<div class="alert alert-danger">
						<strong>{{ session('error') }}</strong>
					</div>
				@else
					<div class="alert alert-warning">Harap cek ulang data yang anda isi. Apakah sudah benar atau belum.</div>
				@endif
                <a href="{{ route('admin.importGuide') }}" class="btn btn-sm sub-header text-white btn-rounded btn-info"><B>Kelik di sini untuk memahami lebih lanjut</B></a>
				<form action="" method="post" enctype="multipart/form-data">
					@csrf
					<div class="row">
					  	<div class="col">
							<div class="form-group">
								<label for="import">Import file:</label>
								<input id="import" type="file" name="import" class="form-control">
							</div>
					  	</div>
					</div>
					<div class="row">
						<div class="col">
							<input class="btn btn-primary" type="submit" value="Upload">
						</div>
					</div>
				</form>
            </div>
        </div>
    </div>
@endsection
