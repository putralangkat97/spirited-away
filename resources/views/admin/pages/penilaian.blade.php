@extends('admin.layouts.master')

@section('title', 'Penilaian')

@section('page-title', 'Penilaian')

@section('content')
    <div class="row">
        <div class="col-xl-12">
            <div class="card-box table-responsive">
                <h4 class="header-title mb-2">Daftar Penilaian</h4>
                <button type="button" class="sub-header btn btn-rounded btn-sm btn-primary text-white" id="tambah"><i class="fa fa-plus"></i></button>
                <table id="datatable" class="table table-bordered  dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead class="text-center">
                        <tr>
                            <th>#</th>
                            <th>Nama Penilaian</th>
                            <th>Level</th>
                            <th>Benar</th>
                            <th>Salah</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody class="text-center">
                        @foreach($penilaians as $key => $penilaian)
                        <tr>
                            <td>{{ ++$key }}</td>
                            <td>{{ $penilaian->nama_penilaian }}</td>
                            <td>{{ $penilaian->level }}</td>
                            <td>{{ $penilaian->benar }}</td>
                            <td>{{ $penilaian->salah }}</td>
                            <td>
                                <a href="#" class="edit btn btn-rounded btn-info" id="{{ $penilaian->id }}" title="Edit"><i class="mdi mdi-pencil"></i></a>
                                &nbsp;
                                <a href="#" class="btn btn-rounded btn-danger" title="Hapus" onclick="deleteConfirmation({{ $penilaian->id }})"><i class="mdi mdi-trash-can"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@include('admin.modals._penilaian')
@endsection

@section('script')
<script src="{{ asset('assets/admin/js/penilaian.js') }}"></script>
<script>
    function pekalian() {
        var a = $('#pointA').val()
        var b = $('#perkalian').val()
        var c = a * b
        console.log(c)
        $('#benar').val(c)
    }

    function pekalianB() {
        var ab = $('#pointB').val()
        var bc = $('#perkalianB').val()
        var cd = ab * bc
        console.log(cd)
        $('#salah').val(cd)
    }
</script>
@endsection