@extends('user.layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="card-box">
                    @if (session()->has('notfound'))
                        <div class="card-body">
                            <div class="alert alert-danger">
                                <strong>{{ session('notfound') }}</strong>
                            </div>
                            <a href="{{ url('/') }}" class="btn btn-rounded btn-primary">Kembali</a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection