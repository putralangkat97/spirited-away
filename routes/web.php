<?php

use UniSharp\LaravelFilemanager\Lfm;

Route::get('/', function () {
    return view('index');
});

Auth::routes(['register' => false]);
// Route::get('/home', 'HomeController@index')->name('home');

////////////
// ADMIN //
///////////
Route::name('admin.')->group(function () {
    // Auth
    Route::namespace('AdminAuth')
        ->group(function () {
            Route::get('/admin/login', 'LoginController@showLoginForm')->name('login');
            Route::post('/admin/login', 'LoginController@login')->name('login.submit');
            Route::post('/admin/logout', 'LoginController@logout')->name('logout');
    });

    // Admin
    Route::namespace('Admin')
        ->group(function () {

            // Dashboard
            Route::get('/admin', 'AdminController@index')
                ->name('home');
            
            // Topik
            Route::get('/admin/topik', 'EventController@topik')
                ->name('topik');
            Route::post('/admin/topik/add', 'EventController@topikAdd')
                ->name('topikAdd');
            Route::get('/admin/topik/edit/{id}', 'EventController@topikEdit')
                ->name('topikEdit');
            Route::post('/admin/topik/update', 'EventController@topikUpdate')
                ->name('topikUpdate');
            Route::post('/admin/topik/delete/{id}', 'EventController@topikDelete')
                ->name('topikDelete');
            Route::post('/admin/topik/aktifkan/{id}', 'EventController@aktifkan')
                ->name('topikAktifkan');
            Route::post('/admin/topik/nonaktifkan/{id}', 'EventController@nonaktifkan')
                ->name('topikNonaktifkan');
        
            // Quiz
            Route::get('/admin/quiz', 'AdminController@quiz')
                ->name('quiz');
            Route::post('/admin/quiz/add', 'AdminController@quizAdd')
                ->name('quizAdd');
            Route::get('/admin/quiz/edit/{id}', 'AdminController@quizEdit')
                ->name('quizEdit');
            Route::post('/admin/quiz/update', 'AdminController@quizUpdate')
                ->name('quizUpdate');
            Route::post('/admin/quiz/delete/{id}', 'AdminController@quizDelete')
                ->name('quizDelete');
        
            // Pertanyaan
            Route::get('/admin/pertanyaan/{id}', 'AdminController@pertanyaan')
                ->name('pertanyaan');
            Route::post('/admin/pertanyaan/image_upload', 'AdminController@pertanyaanImage')
                ->name('upload');
            Route::post('/admin/pertanyaan/add', 'AdminController@pertanyaanAdd')
                ->name('pertanyaanAdd');
            Route::get('/admin/pertanyaan/edit/{id}', 'AdminController@pertanyaanEdit')
                ->name('pertanyaanEdit');
            Route::put('/admin/pertanyaan/update', 'AdminController@pertanyaanUpdate')
                ->name('pertanyaanUpdate');
            Route::delete('/admin/pertanyaan/delete/{id}', 'AdminController@pertanyaanDelete')
                ->name('pertanyaanDelete');
        
            // Jawaban
            Route::get('/admin/jawaban/{id}', 'AdminController@jawaban')
                ->name('jawaban');
            Route::post('/admin/jawaban/add', 'AdminController@jawabanAdd')
                ->name('jawabanAdd');
            Route::get('/admin/jawaban/edit/{id}', 'AdminController@jawabanEdit')
                ->name('jawabanEdit');
            Route::put('/admin/jawaban/update', 'AdminController@jawabanUpdate')
                ->name('jawabanUpdate');
            Route::delete('/admin/jawaban/delete/{id}', 'AdminController@jawabanDelete')
                ->name('jawabanDelete');
        
            // Penilaian
            Route::get('/admin/penilaian', 'AdminController@penilaian')
                ->name('penilaian');
            Route::post('/admin/penilaian/add', 'AdminController@penilaianAdd')
                ->name('penilaianAdd');
            Route::get('/admin/penilaian/edit/{id}', 'AdminController@penilaianEdit')
                ->name('penilaianEdit');
            Route::post('/admin/penilaian/update', 'AdminController@penilaianUpdate')
                ->name('penilaianUpdate');
            Route::post('/admin/penilaian/delete/{id}', 'AdminController@penilaianDelete')
                ->name('penilaianDelete');
        
            // Bank
            Route::get('/admin/bank', 'AdminController@bank')
                ->name('bank');
            Route::post('/admin/bank/add', 'AdminController@bankAdd')
                ->name('bankAdd');
            Route::get('/admin/bank/edit/{id}', 'AdminController@bankEdit')
                ->name('bankEdit');
            Route::post('/admin/bank/update', 'AdminController@bankUpdate')
                ->name('bankUpdate');
            Route::post('/admin/bank/delete/{id}', 'AdminController@bankDelete')
                ->name('bankDelete');
            Route::post('/admin/bank/active/{id}', 'AdminController@bankActive')
                ->name('bankActive');
            Route::post('/admin/bank/inactive/{id}', 'AdminController@bankInActive')
                ->name('bankInActive');
        
            // Export/Import Excel
            Route::get('/admin/user/export/excel', 'AdminController@excel')
                ->name('excel');

            // Tambah Admin
            Route::get('/admin/tambah-admin', 'AdminController@admin')->name('tambahAdmin');
            Route::get('/admin/tambah-admin/edit/{id}', 'AdminController@editAdmin')->name('editAdmin');
            Route::post('/admin/tambah-admin/add', 'AdminController@tambahAdmin')->name('adminAdd');
            Route::post('/admin/tambah-admin/update', 'AdminController@updateAdmin')->name('adminUpdate');
            Route::post('/admin/tambah-admin/delete/{id}', 'AdminController@deleteAdmin')->name('adminDelete');
        
            // User
            Route::get('/admin/user', 'UserController@user')
                ->name('user');
            Route::get('/admin/user/add', 'UserController@userAdd')
                ->name('userAdd');
            Route::post('/admin/user/kota', 'UserController@kota')
                ->name('kota');
            Route::post('/admin/user/insert', 'UserController@userInsert')
                ->name('userInsert');
            Route::post('/admin/user/active/{id}', 'UserController@userActive')
                ->name('userActive');
            Route::post('/admin/user/inactive/{id}', 'UserController@userInactive')
                ->name('userInactive');
            Route::post('/admin/user/delete/{id}', 'UserController@userDelete')
                ->name('userDelete');
            Route::get('/admin/user/detail/{id}', 'UserController@userDetail')
                ->name('userDetail');
        
            // Pembayaran
            Route::get('/admin/pembayaran', 'AdminController@pembayaran')
                ->name('pembayaran');
            Route::get('/admin/pembayaran/bukti/{id}', 'AdminController@pembayaranBukti')
                ->name('pembayaranBukti');
            Route::post('/admin/pembayaran/confirm/{id}', 'AdminController@pembayaranConfirm')
                ->name('pembayaranConfirm');
            Route::post('/admin/pembayaran/batal/{id}', 'AdminController@pembayaranBatal')
                ->name('pembayaranBatal');

            // Sertifikat
            Route::get('/admin/sertifikat', 'AdminController@sertifikat')
            ->name('sertifikat');    
        
            // Leaderboard
            Route::get('/admin/leaderboard', 'LeaderboardController@leaderboard')
                ->name('leaderboard');
            Route::get('/admin/leaderboard/result', 'LeaderboardController@leaderboardResult')
                ->name('leaderboardResult');
        });

    // Import Controller
    Route::get('/admin/user/import/excel', 'ImportController@userImport')
        ->name('import');
    Route::post('/admin/user/import/excel', 'ImportController@importStore')
        ->name('import.submit');
    Route::get('/admin/user/import/panduan', 'ImportController@importGuide')
        ->name('importGuide');
});

Route::group(['prefix' => 'filemanager'], function() {
    Lfm::routes();
});

//////////
// USER //
//////////
Route::name('user.')
    ->group(function () {
        // Auth
        Route::namespace('Auth')
            ->group(function () {
                Route::post('/user/logout', 'LoginController@logoutUser')
                    ->name('logout');
            });
        
        // User
        Route::namespace('User')
            ->group(function () {
                Route::get('/dashboard', 'UserController@index')
                    ->name('dashboard');
                Route::get('/pembayaran', 'UserController@pembayaran')
                    ->name('pembayaran');
                Route::get('/konfirmasi', 'UserController@konfirmasi')
                    ->name('konfirmasi');
                Route::post('/konfirmasi/add', 'UserController@konfirmasiAdd')
                    ->name('konfirmasiAdd');
                Route::get('/status', 'UserController@status')
                    ->name('status');
                Route::get('/exampaper', 'ExampaperController@exampaper')
                    ->name('exampaper');
                Route::get('/exampaper/notfound', 'ExampaperController@notfound')
                    ->name('notfound');
                Route::get('/exampaper/berakhir', 'ExampaperController@berakhir')
                    ->name('berakhir');
                Route::get('/exampaper/kosong', 'ExampaperController@kosong')
                    ->name('kosong');
                Route::get('/lembarsoal/{kode_soal}', 'ExampaperController@lembarsoal')
                    ->name('lembarsoal');
                Route::post('/lembarsoal/submit', 'ExampaperController@storeTest')
                    ->name('storeTest');
            });
});
