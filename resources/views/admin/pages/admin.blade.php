@extends('admin.layouts.master')

@section('title', 'Admin')

@section('page-title', 'Admin')

@section('content')
    <div class="row">
        <div class="col-xl-12">
            <div class="card-box table-responsive">
                <h4 class="header-title mb-2">Daftar Admin</h4>
                <button type="button" class="sub-header btn btn-rounded btn-sm btn-primary text-white" id="tambah"><i class="fa fa-plus"></i></button>
                <table id="datatable" class="table table-bordered  dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead class="text-center">
                        <tr>
                            <th>#</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody class="text-center">
                        @foreach ($admins as $key => $admin)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $admin->name }}</td>
                                <td>{{ $admin->email }}</td>
                                <td>
                                    <button title="Edit" class="edit btn    btn-rounded btn-info" id="{{ $admin->id }}"><i class="mdi mdi-pencil"></i></button>
                                    &nbsp;
                                    <button title="Hapud" class="edit btn    btn-rounded btn-danger" onclick="hapus({{ $admin->id }})"><i class="mdi mdi-trash-can"></i></button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@include('admin.modals._admin')
@endsection

@section('script')
    <script src="{{ asset('assets/admin/js/admin.js') }}"></script>
@endsection