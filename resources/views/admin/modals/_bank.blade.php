<div id="modalBank" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Rekening</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form id="formBank" method="post">
                @csrf
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="nama_bank">Nama Bank:</label>
                                <input type="text" name="nama_bank" id="nama_bank" class="form-control" required placeholder="Nama bank">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="nama_pemilik">Nama Pemegang Akun:</label>
                                <input type="text" name="nama_pemilik" id="nama_pemilik" class="form-control" required placeholder="Nama pemilik">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="no_rekening">No. Rekening</label>
                                <input type="text" name="no_rekening" id="no_rekening" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" class="form-control" required placeholder="No. Rekening">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <input type="hidden" name="hidden_id" id="id">
                            <input type="hidden" name="add" id="tambah" value="Tambah">
                            <input class="btn btn-primary btn-block" type="submit" id="action" value="tambah">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>