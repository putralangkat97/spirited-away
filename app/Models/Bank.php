<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $guarded = [];

    public function pembayarans() {
        return $this->hasMany('App\Models\Pembayaran', 'bank_id');
    }
}
