<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function olimpiades() {
        return $this->belongsToMany('App\Models\Olimpiade');
    }

    public function province() {
        return $this->belongsTo('App\Models\Province', 'provinsi_id');
    }

    public function kecamatan() {
        return $this->belongsTo('App\Models\Kecamatan', 'kabupaten_kota_id');
    }

    public function resultUsers() {
        return $this->hasMany('App\Models\Result', 'user_id');
    }

    public function pembayarans() {
        return $this->hasMany('App\Models\Pembayaran', 'user_id');
    }

    public function userlogs() {
        return $this->hasMany('App\Models\UserLog', 'user_id');
    }
}
