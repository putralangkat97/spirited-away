<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table = 'indonesia_provinces';

    public function users() {
        return $this->hasMany('App\User', 'provinsi_id');
    }

    public function kecamatans() {
        return $this->hasMany('App\Models\Kecamatan', 'province_id');
    }
}
