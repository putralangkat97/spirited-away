@extends('user.layouts.master')

@section('content')
<div class="tes container">
    <div class="row">
        <div class="col-xl-12">
            <div class="card-box">
                @if (Auth::user()->is_active == 0)
                    <h1>Selamat datang {{ Auth::user()->username }}</h1>
                    <h4 class="sub-header">Anda belum bisa mengakses halaman ujian, silahkan bayar terlebih dahulu pada halaman pembayaran</h4>
                    <a href="{{ route('user.pembayaran') }}">Pembayaran</a> | <a href="{{ route('user.status') }}">Status pembayaran</a>
                @else
                    <div class="alert alert-success">Akun anda sudah aktif!</div>
                    <p class="lead">Temukan soal kamu <a href="{{ url('/') }}">disini.</a></p>
                    <br>
                    <h4>History:</h4>
                    <p class="lead">
                        Ujian:
                        <ul>
                            @forelse ($olimpiade as $data)
                                <li>
                                    Olimpiade: {{ $data->olimpiade->nama_olimpiade }}<small>({{ $data->olimpiade->tingkat }})</small>
                                </li>
                                <li>
                                    Point : {{ $data->point }}
                                </li>
                            @empty
                                <li>Olimpiade:</li>
                                <li>Point:</li>
                            @endforelse
                        </ul>
                        <strong>Jumlah point: {{ implode($point) }}</strong>
                    </p>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
