<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>POSI | {{ Auth::user()->username }}</title>
    @include('admin.partials._head')
</head>
<body>
    <!-- Begin page -->
    <div id="wrapper">
        <!-- Topbar Start -->
        <div class="navbar-custom">
            <ul class="list-unstyled topnav-menu float-right mb-0">
                <li class="dropdown notification-list">
                    <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                        <img src="{{ asset('img/male.png') }}" alt="user-image" class="rounded-circle">
                        <span class="pro-user-name ml-1">
                            {{ Auth::user()->username }}  <i class="mdi mdi-chevron-down"></i> 
                        </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                        <!-- item-->
                        <div class="dropdown-header noti-title">
                            <h6 class="text-overflow m-0">Welcome !</h6>
                        </div>
                        <!-- item-->
                        <div class="dropdown-divider"></div>
                        <!-- item-->
                        <a href="{{ route('user.logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="dropdown-item notify-item">
                            <i class="fe-log-out"></i>
                            <span>{{ __('Logout') }}</span>
                        </a>
                        <form id="logout-form" action="{{ route('user.logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
        
                    </div>
                </li>
            </ul>
        
            <!-- LOGO -->
            <div class="logo-box">
                <a href="#" class="logo text-center">
                    <span class="logo-lg">
                        <img src="{{ asset('img/posi.png') }}" alt="" height="50">
                        <!-- <span class="logo-lg-text-light">UBold</span> -->
                    </span>
                    <span class="logo-sm">
                        <!-- <span class="logo-sm-text-dark">U</span> -->
                        <img src="{{ asset('img/posi.png') }}" alt="" height="15">
                    </span>
                </a>
            </div>
        
            <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
                <li>
                    <button class="button-menu-mobile waves-effect waves-light">
                        <i class="fe-menu"></i>
                    </button>
                </li>
            </ul>
        </div>
        <!-- end Topbar -->
        
        <!-- ========== Left Sidebar Start ========== -->
        <div class="left-side-menu">
            <div class="slimscroll-menu">
                <!--- Sidemenu -->
                <div id="sidebar-menu">
                    <ul class="metismenu" id="side-menu">
                        <li class="menu-title">Time left:</li>
                        <li class="menu-title"><span>Navigasi Soal:</span></li>

                    </ul>
                </div>
                <!-- End Sidebar -->
                <div class="clearfix"></div>
            </div>
            <!-- Sidebar -left -->
        </div>
        <!-- Left Sidebar End -->

        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <div class="content"> 
                <!-- Start Content-->
                <div class="container-fluid">
                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box">
                                <h4 class="page-title">@yield('page-title')</h4>
                            </div>
                        </div>
                    </div>     
                    <!-- end page title -->
                    <!-- start row -->
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="card-box">
                                <form action="#" method="post">
                                @foreach ($soals as $soal)
                                    {{-- <h5 class="d-flex justify-content-between align-items-center mb-3"></h5> --}}
                                    @foreach ($soal->pertanyaans as $nn => $p)
                                    <div class="card @if(!$loop->last)mb-3 @endif">
                                        <div class="card-header">
                                            <h6>Pertanyaan No. {{ ++$nn }}</h6>
                                            <h4>{!! $p->pertanyaan !!}</h4>
                                        </div>
                                        @foreach ($p->jawabans as $j)
                                        <div class="card-body">
                                            <input type="hidden" name="question.{{ $p->id }}" value="{{ $p->id }}">
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="jawaban_id" id="option-{{ $j->id }}" value="{{ $j->id }}"
                                                @if(old("questions.$p->id") ==  $j->id)
                                                    checked
                                                @endif>
                                                <label class="form-check-label" for="option-{{ $j->id }}">
                                                    {!! $j->jawaban !!}
                                                </label>
                                            </div>  
                                        </div>
                                        @endforeach
                                    </div>
                                    @endforeach
                                @endforeach
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--- end row -->
                </div> <!-- end container-fluid -->
            </div> <!-- end content -->

            <!-- Footer Start -->
            @include('user.partials._footer')
            <!-- end Footer -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page content -->
        <!-- ============================================================== -->
    </div>
    <!-- END wrapper -->

    <!-- Javascript/Add-ons scripts -->
    @include('user.partials._script')
</body>
</html>