$(document).ready(function () {
    $('#datatable').DataTable()

    $('#tambah').click(function () {
        $('.modal-title').text('Tambah Quiz Baru')
        $('#tambah').val('Tambah')
        $('#action').val('Tambah')
        $('#modalQuiz').modal('show')
        genCode()
    })

    $('#formQuiz').on('submit', function (event) {
        event.preventDefault()
        var id = $(this).attr('id')
        var url = ''

        if ($('#tambah').val() == 'Tambah') {
            url = '/admin/quiz/add'
            text = 'Quiz berhasil ditambahkan'
        }

        if ($('#tambah').val() == 'Edit') {
            url = '/admin/quiz/update'
            text = 'Quiz berhasil diupdate'
        }

        $.ajax({
            url: url,
            data: $(this).serialize(),
            dataType: 'JSON',
            method: 'POST',
            success: function (data) {
                if (data.success) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Sukses',
                        text: text
                    }).then(function () {
                        location.reload()
                    })
                    $('#formQuiz').modal('hide')
                    $('#formQuiz')[0].reset()
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Gagal',
                        text: 'Terdapat kesalahan saat memasukkan data'
                    })
                }
                $('#formQuiz')[0].reset()
                $('#modalQuiz').modal('hide')
            }
        })
    })

    $(document).on('click', '.edit', function () {
        var id = $(this).attr('id')
        $.ajax({
            url: '/admin/quiz/edit/'+id,
            dataType: 'JSON',
            success: function (data) {
                $('#id').val(data.quiz.id)
                $('#olimpiade_id').val(data.quiz.olimpiade_id)
                $('#waktu').val(data.quiz.waktu)
                $('#randomString').val(data.quiz.kode_soal)
                $('#tambah').val('Edit')
                $('#action').val('Update')
                $('#modalQuiz').modal('show')
            }
        })
    })
})

function genCode() {
    var chars = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz';
    var stringLength = 7;
    var randomString = '';
    for (var i = 0; i < stringLength; i++) {
        var rnum = Math.floor(Math.random() * chars.length);
        randomString += chars.substring(rnum, rnum+1);
    }

    $('#randomString').val(randomString);
}

function deleteConfirmation(id) {
    Swal.fire({
        icon: 'warning',
        title: "Yang bener..?",
        text: "Dengan menghapus data quiz ini maka akan menghapus semua pertanyaan dan jawaban terkait",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Batal",
        reverseButtons: !0
    }).then(function (event) {

        if (event.value === true) {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                type: 'POST',
                url: '/admin/quiz/delete/'+id,
                data: { _token: CSRF_TOKEN },
                dataType: 'JSON',
                success: function (results) {
                    if (results.success === true) {
                        Swal.fire({
                            // toast: true,
                            icon: 'success',
                            title: "Success",
                            text: "Data Deleted",
                            showConfirmButton: true,
                            timer: 1500
                        }).then(function() {
                            location.reload();
                        });
                        $('#sample_form')[0].reset();
                    } else {
                        Swal.fire("Error!", results.message, "error");
                    }
                }
            });

        } else {
            e.dismiss;
        }

    }, function (dismiss) {
        return false;
    })
}