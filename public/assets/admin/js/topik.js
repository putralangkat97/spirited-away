$(document).ready(function () {
    $('#datatable').DataTable()
    
    $('#start_date').datetimepicker({
        format: 'yyyy-mm-dd hh:ii'
    })

    $('#end_date').datetimepicker({
        format: 'yyyy-mm-dd hh:ii'
    })

    $('#tambah').click(function () {
        $('.modal-title').text('Tambah Event-Bidang Baru')
        $('#tambah').val('Tambah')
        $('#action').val('Tambah')
        $('#modalTopik').modal('show')
    })

    $('#formTopik').on('submit', function (event) {
        event.preventDefault()
        var id = $(this).attr('id')
        var url = ''

        if ($('#tambah').val() == 'Tambah') {
            url = '/admin/topik/add'
            text = 'Topik berhasil ditambahkan'
        }

        if ($('#tambah').val() == 'Edit') {
            url = '/admin/topik/update'
            text = 'Topik berhasil diupdate'
        }

        $.ajax({
            url: url,
            data: $(this).serialize(),
            dataType: 'JSON',
            method: 'POST',
            success: function (data) {
                if (data.success) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Sukses',
                        text: text,
                    }).then(function () {
                        location.reload()
                    })
                    $('#modalTopik').modal('hide')
                    $('#formTopik')[0].reset()
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Gagal',
                        text: 'Terdapat kesalahan saat memasukkan data'
                    })
                }
                $('#formTopik')[0].reset()
                $('#modalTopik').modal('hide')
            }
        })
    })

    $(document).on('click', '.edit', function () {
        var id = $(this).attr('id')
        $.ajax({
            url: '/admin/topik/edit/'+id,
            dataType: 'JSON',
            success: function (data) {
                $('#id').val(data.topik.id)
                $('#nama_olimpiade').val(data.topik.nama_olimpiade)
                $('#tingkat').val(data.topik.tingkat)
                $('#deskripsi').val(data.topik.deskripsi)
                $('#start_date').val(data.topik.start_date)
                $('#end_date').val(data.topik.end_date)
                $('.modal-title').text('Update Informasi Event-Bidang')
                $('#tambah').val('Edit')
                $('#action').val('Update')
                $('#modalTopik').modal('show')
            }
        })
    })
})

function deleteConfirmation(id) {
    Swal.fire({
        icon: 'warning',
        title: "Yang bener..?",
        text: "Dengan menghapus topik ini maka akan menghapus semua Quiz, Pertanyaan, dan Jawaban yang berkategorikan Topik ini.",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Yakin, Hapus semua",
        cancelButtonText: "Batal",
        reverseButtons: !0
    }).then(function (event) {

        if (event.value === true) {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                type: 'POST',
                url: '/admin/topik/delete/'+id,
                data: { _token: CSRF_TOKEN },
                dataType: 'JSON',
                success: function (results) {
                    if (results.success === true) {
                        Swal.fire({
                            // toast: true,
                            icon: 'success',
                            title: "Success",
                            text: "Data Deleted",
                            showConfirmButton: true,
                            timer: 1500
                        }).then(function() {
                            location.reload();
                        });
                        $('#sample_form')[0].reset();
                    } else {
                        Swal.fire("Error!", results.message, "error");
                    }
                }
            });

        } else {
            e.dismiss;
        }

    }, function (dismiss) {
        return false;
    })
}

function aktifkan(id) {
    Swal.fire({
        icon: 'warning',
        title: "Yakin?",
        text: "Apakah anda ingin mengaktifkan event ini",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Aktifkan",
        cancelButtonText: "Batal",
        reverseButtons: !0
    }).then(function (event) {

        if (event.value === true) {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                type: 'POST',
                url: '/admin/topik/aktifkan/'+id,
                data: { _token: CSRF_TOKEN },
                dataType: 'JSON',
                success: function (results) {
                    if (results.success === true) {
                        Swal.fire({
                            // toast: true,
                            icon: 'success',
                            title: "Success",
                            text: "Event diaktifkan",
                            showConfirmButton: true,
                            timer: 1500
                        }).then(function() {
                            location.reload();
                        });
                        $('#sample_form')[0].reset();
                    } else {
                        Swal.fire("Error!", results.message, "error");
                    }
                }
            });

        } else {
            e.dismiss;
        }

    }, function (dismiss) {
        return false;
    })
}

function nonaktifkan(id) {
    Swal.fire({
        icon: 'warning',
        title: "Yakin?",
        text: "Apakah anda ingin me-nonaktifkan event ini",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Non-aktifkan",
        cancelButtonText: "Batal",
        reverseButtons: !0
    }).then(function (event) {

        if (event.value === true) {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                type: 'POST',
                url: '/admin/topik/nonaktifkan/'+id,
                data: { _token: CSRF_TOKEN },
                dataType: 'JSON',
                success: function (results) {
                    if (results.success === true) {
                        Swal.fire({
                            // toast: true,
                            icon: 'success',
                            title: "Success",
                            text: "Event di non-aktifkan",
                            showConfirmButton: true,
                            timer: 1500
                        }).then(function() {
                            location.reload();
                        });
                        $('#sample_form')[0].reset();
                    } else {
                        Swal.fire("Error!", results.message, "error");
                    }
                }
            });

        } else {
            e.dismiss;
        }

    }, function (dismiss) {
        return false;
    })
}