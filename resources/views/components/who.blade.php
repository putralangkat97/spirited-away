@if(Auth::guard('web')->check())
    <p class="text-success">
        You're Logged in as a <strong>User</strong>
    </p>
    @else
    <p class="text-danger">
        You're Logged out as a <strong>User</strong>
    </p>
@endif

@if(Auth::guard('admin')->check())
    <p class="text-success">
        You're Logged in as an <strong>Admin</strong>
    </p>
    @else
    <p class="text-danger">
        You're Logged out as an <strong>Admin</strong>
    </p>
@endif