@extends('admin.layouts.master')

@section('title', 'Event-Bidang')

@section('page-title', 'Event-Bidang')

@section('style')
    <style type="text/css">
        /* .start_date {
            z-index:1151 !important;
        } */
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-xl-12">
            <div class="card-box table-responsive">
                <h4 class="header-title mb-2">Event-Bidang</h4>
                <button type="button" class="sub-header btn btn-rounded btn-sm btn-primary text-white" id="tambah"><i class="fa fa-plus"></i></button>
                <table id="datatable" class="table table-bordered  dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead class="text-center">
                        <tr>
                            <th>#</th>
                            <th>Event-Bidang</th>
                            <th>Tingkat</th>
                            <th>Mulai</th>
                            <th>Selesai</th>
                            <th>Deskripsi</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody class="text-center">
                        @foreach($olimpiades as $key => $o)
                        <tr>
                            <td>{{ ++$key }}</td>
                            <td>{{ $o->id }} - {{ $o->nama_olimpiade }}</td>
                            <td>{{ $o->tingkat }}</td>
                            <td>{{ date('d F Y | H:i', strtotime($o->start_date)) }}</td>
                            <td>{{ date('d F Y | H:i', strtotime($o->end_date)) }}</td>
                            <td>{{ $o->deskripsi }}</td>
                            <td>
                                @if ($o->is_active == 0)
                                    <button class="btn btn-danger btn-sm" onclick="aktifkan({{ $o->id }})">Non-Aktif</button>
                                @else
                                    <button class="btn btn-success btn-sm" onclick="nonaktifkan({{ $o->id }})">Aktif</button>
                                @endif
                            </td>
                            <td>
                                <a href="#" class="edit btn btn-rounded btn-info" id="{{ $o->id }}"><i class="mdi mdi-pencil"></i></a>
                                &nbsp;
                                <a href="#" class="hapus btn btn-rounded btn-danger" onclick="deleteConfirmation({{ $o->id }})"><i class="mdi mdi-trash-can"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@include('admin.modals._topik')
@endsection

@section('script')
<script src="{{ asset('assets/admin/js/topik.js') }}"></script>
@endsection