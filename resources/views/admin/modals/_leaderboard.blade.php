<div class="row">
    <div class="col-xl-12">
        <div class="card-box">
            <div class="row">
                <div class="col-12">
                    <form class="form-inline" action="{{ route('admin.leaderboardResult') }}" method="GET">
                        @csrf
                        <div class="form-group mb-2">
                            <label for="staticEmail2">Sort by:</label>
                        </div>
                        <div class="form-group mx-sm-3 mb-2">
                            <label class="sr-only">Password</label>
                            <select class="form-control" name="event">
                                @forelse ($events as $event)
                                    <option value="{{ $event->id }}">{{ $event->nama_olimpiade }} - {{ $event->tingkat }}</option>
                                @empty
                                    <option selected>Belum ada event.</option>
                                @endforelse
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary mb-2" {{ empty($event) ? 'disabled' : '' }}>Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>