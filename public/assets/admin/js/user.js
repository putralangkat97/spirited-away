$(document).ready(function () {
    $('#datatable').DataTable()

    $('#tambah').click(function () {
        $('.modal-title').text('Tambah User Baru')
        $('#tambah').val('Tambah')
        $('#action').val('Tambah')
        $('#modalUserAdd').modal('show')
    })

    $(document).on('click', '.detail', function () {
        var id = $(this).attr('id')
        $.ajax({
            url: '/admin/user/detail/'+id,
            dataType: 'JSON',
            success: function (data) {
                var province = data.user.province
                var kec      = data.user.kecamatan
                $('.modal-title').text('Detail user ('+data.user.nama_lengkap+')')
                $('#nama_lengkap').text(data.user.nama_lengkap)
                $('#jenis_kelamin').text(data.user.jenis_kelamin)
                $('#tanggal_lahir').text(data.user.tanggal_lahir)
                $('#tempat_lahir').text(data.user.tempat_lahir)
                $('#alamat').text(data.user.alamat)
                $('#no_telepon_whatsapp').text(data.user.no_telepon_whatsapp)
                $('#asal_sekolah').text(data.user.asal_sekolah)
                $('#provinsi_id').text(province.name)
                $('#kabupaten_kota_id').text(kec.name)
                $('#kelas').text(data.user.kelas)
                $('#email').text(data.user.email)
                if (data.user.is_active == 0) {
                    var active = 'Tidak Aktif'
                } else {
                    var active = 'Aktif'
                }
                $('#is_active').text(active)
                $('#modalUser').modal('show')
            }
        })
    })
})

function active(id) {
    Swal.fire({
        icon: 'warning',
        title: "Yang bener..?",
        text: "Anda yakin ingin mengaktifkan akun ini?",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Aktifkan",
        cancelButtonText: "Batal",
        reverseButtons: !0
    }).then(function (event) {
        if (event.value === true) {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                type: 'POST',
                url: '/admin/user/active/'+id,
                data: { _token: CSRF_TOKEN },
                dataType: 'JSON',
                success: function (results) {
                    if (results.success === true) {
                        Swal.fire({
                            // toast: true,
                            icon: 'success',
                            title: "Success",
                            text: "Akun Sudah Aktif",
                            showConfirmButton: true,
                            timer: 1500
                        }).then(function() {
                            location.reload();
                        });
                    } else {
                        Swal.fire("Error!", results.message, "error");
                    }
                }
            });

        } else {
            e.dismiss;
        }

    }, function (dismiss) {
        return false;
    })
}

function inactive(id) {
    Swal.fire({
        icon: 'warning',
        title: "Yang bener..?",
        text: "Anda yakin ingin men-suspend akun ini?",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Suspend",
        cancelButtonText: "Batal",
        reverseButtons: !0
    }).then(function (event) {
        if (event.value === true) {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                type: 'POST',
                url: '/admin/user/inactive/'+id,
                data: { _token: CSRF_TOKEN },
                dataType: 'JSON',
                success: function (results) {
                    if (results.success === true) {
                        Swal.fire({
                            // toast: true,
                            icon: 'success',
                            title: "Success",
                            text: "Akun Berhasil di Non-Aktifkan",
                            showConfirmButton: true,
                            timer: 1500
                        }).then(function() {
                            location.reload();
                        });
                    } else {
                        Swal.fire("Error!", results.message, "error");
                    }
                }
            });

        } else {
            e.dismiss;
        }

    }, function (dismiss) {
        return false;
    })
}

function deleteConfirmation(id) {
    Swal.fire({
        icon: 'warning',
        title: "Yang bener..?",
        text: "Anda yakin ingin menghapus user ini?",
        showCancelButton: !0,
        confirmButtonText: "Hapus",
        cancelButtonText: "Batal",
        reverseButtons: !0
    }).then(function (event) {
        if (event.value === true) {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                type: 'POST',
                url: '/admin/user/delete/'+id,
                data: { _token: CSRF_TOKEN },
                dataType: 'JSON',
                success: function (results) {
                    if (results.success === true) {
                        Swal.fire({
                            // toast: true,
                            icon: 'success',
                            title: "Success",
                            text: "Data Deleted",
                            showConfirmButton: true,
                            timer: 1500
                        }).then(function() {
                            location.reload();
                        });
                        $('#sample_form')[0].reset();
                    } else {
                        Swal.fire("Error!", results.message, "error");
                    }
                }
            });

        } else {
            e.dismiss;
        }

    }, function (dismiss) {
        return false;
    })
}