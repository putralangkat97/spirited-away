$(document).ready(function () {
    $('#datatable').DataTable()

    $('#tambah').click(function () {
        $('.modal-title').text('Tambah Kategori Penilaian')
        $('#tambah').val('Tambah')
        $('#action').val('Tambah')
        $('#modalPenilaian').modal('show')
    })
    
    $('#formPenilaian').on('submit', function (event) {
        event.preventDefault();
        var id = $(this).attr('id')
        var url = ''

        if ($('#tambah').val() == 'Tambah') {
            url = '/admin/penilaian/add'
            text = 'Penilaian berhasil ditambahkan'
        }

        if ($('#tambah').val() == 'Edit') {
            url = '/admin/penilaian/update'
            text = 'Penilaian berhasil diupdate'
        }

        $.ajax({
            url: url,
            data: $(this).serialize(),
            dataType: 'JSON',
            method: 'POST',
            success: function (data) {
                if (data.success) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Sukses',
                        text: text
                    }).then(function () {
                        location.reload()
                    })
                    $('#formPenilaian').modal('hide')
                    $('#formPenilaian')[0].reset()
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Gagal',
                        text: 'Terdapat kesalahan saat memasukkan data'
                    }).then(function () {
                        $('#formPenilaian')[0].reset()
                        $('#modalPenilaian').modal('hide')
                    })
                }
            }
        })
    })

    $(document).on('click', '.edit', function () {
        var id = $(this).attr('id')
        $.ajax({
            url: '/admin/penilaian/edit/'+id,
            dataType: 'JSON',
            success: function (data) {
                $('#id').val(data.penilaian.id)
                $('#nama_penilaian').val(data.penilaian.nama_penilaian)
                $('#level').val(data.penilaian.level)
                $('#benar').val(data.penilaian.benar)
                $('#salah').val(data.penilaian.salah)
                $('.modal-title').text('Update Informasi Penilaian')
                $('#tambah').val('Edit')
                $('#action').val('Update')
                $('#modalPenilaian').modal('show')
            }
        })
    })
})

function deleteConfirmation(id) {
    Swal.fire({
        icon: 'warning',
        title: "Peringatan!",
        text: "Dengan menghapus penilaian maka semua jawaban yang terkait akan terhapus",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Batal",
        reverseButtons: !0
    }).then(function (event) {

        if (event.value === true) {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                type: 'POST',
                url: '/admin/penilaian/delete/'+id,
                data: { _token: CSRF_TOKEN },
                dataType: 'JSON',
                success: function (results) {
                    if (results.success === true) {
                        Swal.fire({
                            // toast: true,
                            icon: 'success',
                            title: "Success",
                            text: "Data Deleted",
                            showConfirmButton: true,
                            timer: 1500
                        }).then(function() {
                            location.reload();
                        });
                        $('#sample_form')[0].reset();
                    } else {
                        Swal.fire("Error!", results.message, "error");
                    }
                }
            });

        } else {
            e.dismiss;
        }

    }, function (dismiss) {
        return false;
    })
}