@extends('admin.layouts.master')

@section('title', 'POSI Dashboard')

@section('page-title', 'Dashboard')

@section('content')
<div class="row">
    <div class="col-xl-4 col-sm-6">
        <div class="card-box widget-box-two widget-two-custom">
            <div class="media">
                <div class="avatar-lg rounded-circle bg-primary widget-two-icon align-self-center">
                    <i class="mdi mdi-account-multiple avatar-title font-30 text-white"></i>
                </div>
                <div class="wigdet-two-content media-body">
                    <p class="m-0 text-uppercase font-weight-medium text-truncate" title="Statistics">Peserta Terdaftar</p>
                    <h3 class="font-weight-medium my-2"><i class="mdi mdi-account-multiple"></i> <span data-plugin="counterup">{{ $total }}</span></h3>
                    <p class="m-0"><small>Data Terakhir: </small>{{ $dataLast == '' ? '-' : date('d F Y', strtotime($dataLast->created_at)) }}</p>
                </div>
            </div>
        </div>
    </div>
    <!-- end col -->
    <div class="col-xl-4 col-sm-6">
        <div class="card-box widget-box-two widget-two-custom ">
            <div class="media">
                <div class="avatar-lg rounded-circle bg-primary widget-two-icon align-self-center">
                    <i class="mdi mdi-gender-male avatar-title font-30 text-white"></i>
                </div>
                <div class="wigdet-two-content media-body">
                    <p class="m-0 text-uppercase font-weight-medium text-truncate" title="Statistics">Peserta Laki-Laki</p>
                    <h3 class="font-weight-medium my-2"><i class="mdi mdi-human-male"></i> <span data-plugin="counterup">{{ $laki }}</span></h3>
                    <p class="m-0"><small>Data Terakhir: </small>{{ $lakilast == '' ? '-' : date('d F Y', strtotime($lakilast->created_at)) }}</p>
                </div>
            </div>
        </div>
    </div>
    <!-- end col -->
    <div class="col-xl-4 col-sm-6">
        <div class="card-box widget-box-two widget-two-custom ">
            <div class="media">
                <div class="avatar-lg rounded-circle bg-primary widget-two-icon align-self-center">
                    <i class="mdi mdi-gender-female avatar-title font-30 text-white"></i>
                </div>
                <div class="wigdet-two-content media-body">
                    <p class="m-0 text-uppercase font-weight-medium text-truncate" title="Statistics">Peserta Perempuan</p>
                    <h3 class="font-weight-medium my-2"><i class="mdi mdi-human-female"></i><span data-plugin="counterup">{{ $puan }}</span></h3>
                    <p class="m-0"><small>Data Terakhir: </small>{{ $puanlast == '' ? '-' : date('d F Y', strtotime($puanlast->created_at)) }}</p>
                </div>
            </div>
        </div>
    </div>
    <!-- end col -->
</div>
<div class="row">
    <div class="col-xl-12 col-sm-12">
        <div class="card-box">
            <h4 class="header-title">Rankings</h4>
            <p class="sub-title">Siswa dengan nilai tertinggi</p>
            <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                <thead class="text-center">
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Asal Sekolah</th>
                        <th>Point</th>
                    </tr>
                </thead>
                <tbody class="text-center">
                    @php
                        $no = 0;
                    @endphp
                    @foreach ($users as $user)
                    @php
                        $no++;
                    @endphp
                    <tr>
                        <td>{{ $no }}</td>
                        <td>{{ $user->nama_lengkap }}</td>
                        <td>{{ $user->asal_sekolah }}</td>
                        <td>{{ $user->jumlah_point }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-xl-12 col-sm-12">
        <div class="card-box">
            <h4 class="header-title">Charts</h4><br>
            <div id="charts"></div>
        </div>
    </div>
</div>
<!-- end row -->    
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $('#datatable').DataTable({
                'ordering': false
            });
        });
    </script>
    <script src="{{ asset('js/highcharts.src.js') }}"></script>
    <script>
        Highcharts.chart('charts', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Total Peserta Olimpiade'
            },
            subtitle: {
                text: 'Tahun {{ date('Y') }}'
            },
            xAxis: {
                categories: [
                    'Jan',
                    'Feb',
                    'Mar',
                    'Apr',
                    'May',
                    'Jun',
                    'Jul',
                    'Aug',
                    'Sep',
                    'Oct',
                    'Nov',
                    'Dec'
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Jumlah Siswa'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'SD',
                data: [
                    {{ $janSD }}, {{ $febSD }}, {{ $marSD }}, {{ $aprSD }}, {{ $meiSD }}, {{ $junSD }}, {{ $julSD }}, {{ $aguSD }}, {{ $sepSD }}, {{ $oktSD }}, {{ $novSD }}, {{ $desSD }}
                ]

            }, {
                name: 'SMP',
                data: [
                    {{ $janSMP }}, {{ $febSMP }}, {{ $marSMP }}, {{ $aprSMP }}, {{ $meiSMP }}, {{ $junSMP }}, {{ $julSMP }}, {{ $aguSMP }}, {{ $sepSMP }}, {{ $oktSMP }}, {{ $novSMP }}, {{ $desSMP }}
                ]

            }, {
                name: 'SMA',
                data: [
                    {{ $janSMA }}, {{ $febSMA }}, {{ $marSMA }}, {{ $aprSMA }}, {{ $meiSMA }}, {{ $junSMA }}, {{ $julSMA }}, {{ $aguSMA }}, {{ $sepSMA }}, {{ $oktSMA }}, {{ $novSMA }}, {{ $desSMA }}
                ]
            }]
        });
    </script>
@endsection
