<div id="modalTopik" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Topik</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form id="formTopik" method="post" data-parsley-validate>
                @csrf
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="nama_olimpiade">Event-Bidang:</label>
                                <input type="text" name="nama_olimpiade" id="nama_olimpiade" class="form-control" placeholder="Event-Bidang" autocomplete="off" required data-parsley-required-message="Event-Bidang harus di isi!">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="tingkat">Tingkat:</label>
                                <select name="tingkat" id="tingkat" class="form-control" required>
                                    <option value="">Pilih</option>
                                    <option value="siswaSD">SD (Siswa)</option>
                                    <option value="siswaSMP">SMP (Siswa)</option>
                                    <option value="siswaSMA">SMA (Siswa)</option>
                                    <option value="guruSD">SD (Guru)</option>
                                    <option value="guruSMP">SMP (Guru)</option>
                                    <option value="guruSMA">SMA (Guru)</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="start_date">Tanggal Mulai:</label>
                                <input type="text" name="start_date" id="start_date" class="start_date form-control" placeholder="Tanggal Mulai Event">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="end_date">Tanggal Selesai:</label>
                                <input type="text" name="end_date" id="end_date" class="end_date form-control" placeholder="Tanggal Selesai Event">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="deskripsi">Deskripsi:</label>
                                <textarea class="form-control" name="deskripsi" id="deskripsi" cols="10" rows="4" required placeholder="Deskripsi Event" data-parsley-required-message="Deskripsi tidak boleh kosong!"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <input type="hidden" name="hidden_id" id="id">
                            <input type="hidden" name="add" id="tambah" value="Tambah">
                            <input class="btn btn-primary btn-block" type="submit" id="action" value="tambah">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>