@extends('admin.layouts.master')

@section('title', 'Petunjuk')

@section('page-title', 'Petunjuk Pengisian')

@section('content')
    <div class="row">
        <div class="col-xl-12">
            <div class="card-box">
				<h4 class="header-title mb-2">Silahkan baca dan ikuti terlebih dahulu petunjuk di bawah ini</h4>
                <a href="{{ route('admin.import') }}" class="btn text-white btn-sm btn-rounded btn-success sub-header"><B>Kembali</B></a>
            </div>
        </div>
	</div>

@endsection