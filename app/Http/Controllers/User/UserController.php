<?php

namespace App\Http\Controllers\User;

use App\User;
use App\Models\Bank;
use App\Models\Quiz;
use App\Models\Result;
use App\Models\Jawaban;
use App\Models\UserLog;
use App\Models\Olimpiade;
use App\Models\Pembayaran;
use App\Models\Pertanyaan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\Console\Input\Input;

class UserController extends Controller
{
    public function __construct() {
        $this->middleware('auth:web');
    }

    public function index() {
        $jumlahPoint = User::where('id', Auth::user()->id)->get()->pluck('jumlah_point')->toArray();
        $statusPembayaran = Pembayaran::where('user_id', Auth::user()->id)
                                ->first();
        $results = UserLog::where('user_id', Auth::user()->id)->with('olimpiade')->get();

        // dd($results);
        return view('user.index', [
            'point' => $jumlahPoint,
            'olimpiade' => $results,
            'status' => $statusPembayaran
        ]);
    }

    public function pembayaran() {
        $banks = Bank::orderBy('is_active', 'desc')->get();
        return view('user.pages.pembayaran')->with('banks', $banks);
    }

    public function konfirmasi() {
        $banks = Bank::orderBy('id', 'desc')->get();
        return view('user.pages.konfirmasi', ['banks' => $banks]);
    }

    public function konfirmasiAdd(Request $request) {

        $lastNumber = Pembayaran::orderBy('id', 'desc')->first()->no_pembayaran ?? 0;
        $lastIncrement = substr($lastNumber, -3);
        $newNumber = 'PO'.date('dmY').str_pad($lastIncrement + 1, 4, 0, STR_PAD_LEFT);

        $file               = $request->file('image');
        $filename           = time().'.'.$file->getClientOriginalExtension();
        $file->move(('bukti'), $filename);

        $userId = Auth::user()->id;

        $konfirmasi = array(
            'user_id' => $userId,
            'no_pembayaran' => $newNumber,
            'bank_id' => $request->bank_id,
            'pemilik_rekening' => $request->pemilik_rekening,
            'nominal' => $request->nominal,
            'tanggal_transfer' => $request->tanggal_transfer,
            'image' => $filename
        );

        Pembayaran::create($konfirmasi);

        return redirect()->route('user.status');
    }

    public function status() {
        $data = Pembayaran::where('user_id', Auth::user()->id)->get();
        foreach ($data as $v) {
            $v;
        }

        if (empty($v)) {
            return view('user.pages.belumbayar')->with('belum', 'Maaf kamu belum ada melakukan pembayaran.');
        } else {
            return view('user.pages.status', [
                'data' => $v
            ]);
        }
    }
}
