<?php

namespace App\Imports;

use App\User;
use Carbon\Carbon;
use App\Models\Olimpiade;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToModel;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class UserImport implements ToModel, WithHeadingRow, SkipsOnError
{
    use Importable, SkipsErrors;

    public $userImport;
    public $topik;

    public function transformDate($value, $format = 'Y-m-d') {
        try {
            return \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value));
        } catch (\ErrorException $e) {
            return \Carbon\Carbon::createFromFormat($format, $value);
        }
    }

    public function model(array $row) {
        $this->userImport = User::firstOrCreate(
            ['email' => $row['email']], [
            'nama_lengkap' => $row['nama_lengkap'],
            'username' => $row['username'],
            'tanggal_lahir' => $this->transformDate($row['tanggal_lahir']),
            'tempat_lahir' => $row['tempat_lahir'],
            'jenis_kelamin' => $row['jenis_kelamin'],
            'alamat' => $row['alamat'],
            'no_telepon_whatsapp' => $row['no_telp'],
            'provinsi_id' => $row['provinsi'],
            'kabupaten_kota_id' => $row['kabupaten_kota'],
            'asal_sekolah' => $row['asal_sekolah'],
            'kelas' => $row['kelas'],
            'password' => bcrypt($row['password']),
            'is_active' => $row['status'],
        ]);
        $this->topik = Olimpiade::where('id', '=', $row['bidang'])->pluck('id')->toArray();

        $this->test();
    }

    public function test() {
        $this->userImport->olimpiades()->attach($this->topik);
    }

    public function onError(\Throwable $error) {

    }
}
