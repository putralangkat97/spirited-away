<div id="modalUser" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Topik</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <tr>
                        <th>Nama:</th>
                        <td id="nama_lengkap"></td>
                        <th rowspan=6></th>
                        <th>Asal Sekolah:</th>
                        <td id="asal_sekolah"></td>
                    </tr>
                    <tr>
                        <th>Jenis Kelamin:</th>
                        <td id="jenis_kelamin"></td>
                        <th>Provinsi:</th>
                        <td id="provinsi_id"></td>
                    </tr>
                    <tr>
                        <th>Tanggal Lahir:</th>
                        <td id="tanggal_lahir"></td>
                        <th>Kecamatan/Kota:</th>
                        <td id="kabupaten_kota_id"></td>
                    </tr>
                    <tr>
                        <th>Tempat Lahir:</th>
                        <td id="tempat_lahir"></td>
                        <th>Kelas:</th>
                        <td id="kelas"></td>
                    </tr>
                    <tr>
                        <th>Alamat:</th>
                        <td id="alamat"></td>
                        <th>E-mail:</th>
                        <td id="email"></td>
                    </tr>
                    <tr>
                        <th>No. Telp/Whatsapp:</th>
                        <td id="no_telepon_whatsapp"></td>
                        <th>Status:</th>
                        <td id="is_active"></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>