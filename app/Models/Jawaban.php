<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Jawaban extends Model
{
    protected $guarded = [];

    public function pertanyaan() {
        return $this->belongsTo('App\Models\Pertanyaan', 'pertanyaan_id');
    }

    public function penilaian() {
        return $this->belongsTo('App\Models\Penilaian', 'penilaian_id');
    }

    public function resultJawabans() {
        return $this->belongsToMany('App\Models\Result', 'jawaban_id');
    }
}
