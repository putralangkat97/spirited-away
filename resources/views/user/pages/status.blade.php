@extends('user.layouts.master')

@section('content')
    <div class="container">
        <div class="col-xl-12 col-md-12">
            <div class="card-box table-responsive">
                @if ($data->status == 0)
                    <div class="alert alert-warning">
                        <span><strong>Berhasil mengirim bukti pembayaran, silahkan tunggu hingga admin mengkonfirmasi dan mengaktifkan akun anda. <a href="{{ route('user.dashboard') }}">Kembali</a></strong></span>
                    </div>
                @else
                    <div class="alert alert-success">
                        <span><strong>Pembayaran anda telah dikonfirmasi dan akun kamu telah aktif! Sekarang kamu bisa mengikuti ujian. <a href="{{ route('user.dashboard') }}">Kembali</a></strong></span>
                    </div>
                @endif
                <table id="datatable" class="table table-bordered  dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead class="text-center bg-success text-white">
                        <tr>
                            <th>Tgl. Transfer</th>
                            <th>No. Pembayaran</th>
                            <th>Rek. Tujuan</th>
                            <th>Atas Nama</th>
                            <th>Nominal</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody class="text-center">
                        <tr>
                        <td>{{ date('d-m-Y', strtotime($data->tanggal_transfer)) }}</td>
                        <td>{{ $data->no_pembayaran }}</td>
                        <td>{{ $data->bank->nama_bank }}</td>
                        <td>{{ $data->pemilik_rekening }}</td>
                        <td>Rp. {{ $data->nominal }}</td>
                        @if ($data->status == 0)
                            <td class="text-danger"><b>Menunggu konfirmasi</b></td>
                            @else
                            <td class="text-success">Pembayaran Sukses</td>
                        @endif
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection