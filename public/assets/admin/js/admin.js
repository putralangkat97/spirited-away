$(document).ready(function () {
    $('#datatable').DataTable({
        'ordering': false
    })

    $('#tambah').click( function () {
        $('.modal-title').text('Tambah Admin')
        $('#tambah').val('Tambah')
        $('#action').val('Tambah')
        $('#modalAdmin').modal('show')
    })

    $('#formAdmin').on('submit', function (event) {
        event.preventDefault()
        var id = $(this).attr('id')
        var url = ''

        if ($('#tambah').val() == 'Tambah') {
            url = '/admin/tambah-admin/add'
            text = 'Admin berhasil ditambahkan'
        }

        if ($('#tambah').val() == 'Edit') {
            url = '/admin/tambah-admin/update'
            text = 'Admin berhasil di update'
        }

        $.ajax({
            url: url,
            data: $(this).serialize(),
            dataType: 'JSON',
            method: 'POST',
            success: function (data) {
                if (data.success) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Sukses',
                        text: text
                    }).then(function () {
                        location.reload()
                    })
                    $('#formAdmin').modal('hide')
                    $('#formAdmin')[0].reset()
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Gagal',
                        text: 'Terdapat kesalahan saat memasukkan data'
                    }).then(function () {
                        $('#formAdmin')[0].reset()
                        $('#modalAdmin').modal('hide')
                    })
                }
            }
        })
    })

    $(document).on('click', '.edit', function () {
        var id = $(this).attr('id')
        $.ajax({
            url: '/admin/tambah-admin/edit/'+id,
            dataType: 'JSON',
            success: function (data) {
                $('#nama_admin').val(data.admin.name)
                $('#email').val(data.admin.email)
                $('#id').val(data.admin.id)
                $('.modal-title').text('Edit Informasi Admin')
                $('#tambah').val('Edit')
                $('#action').val('Update')
                $('#modalAdmin').modal('show')
            }
        })
    })
})

function check() {
    var pass = $('#password').val();
    var conpass = $('#confirm-password').val();

    if (pass != conpass) {
        $('#tes').html('Password tidak cocok!').addClass('text-danger');
    } else {
        $('#tes').html('Password cocok').removeClass('text-danger').addClass('text-success');
    }
}

function hapus(id) {
    Swal.fire({
        icon: 'warning',
        title: "Yang bener..?",
        text: "Anda yakin ingin mneghapus admin ini?",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Hapus",
        cancelButtonText: "Batal",
        reverseButtons: !0
    }).then(function (event) {

        if (event.value === true) {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                type: 'POST',
                url: '/admin/tambah-admin/delete/'+id,
                data: { _token: CSRF_TOKEN },
                dataType: 'JSON',
                success: function (results) {
                    if (results.success === true) {
                        Swal.fire({
                            // toast: true,
                            icon: 'success',
                            title: "Success",
                            text: "Admin berhasil di hapus!",
                            showConfirmButton: true,
                            timer: 1500
                        }).then(function() {
                            location.reload();
                        });
                        $('#sample_form')[0].reset();
                    } else {
                        Swal.fire("Error!", results.message, "error");
                    }
                }
            });

        } else {
            e.dismiss;
        }

    }, function (dismiss) {
        return false;
    })
}