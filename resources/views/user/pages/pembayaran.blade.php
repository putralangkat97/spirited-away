@extends('user.layouts.master')

@section('content')
<div class="container">
    <div class="py-5 text-center">
      <img class="d-block mx-auto mb-4" src="{{ asset('img/posi.png') }}" alt="Logo Posi" width="340">
      <h2>Halaman Info Pembayaran</h2>
      <p class="lead">Setelah melakukan pembayaran harap simpan bukti/struk transfer dari ATM. Hal ini berguna agar mempercepat proses pengaktifan akun. <strong>Semua reking atas nama PT. POSI. <span class="bg-secondary">Harap transfer ke rekening yang sedang aktif "<span><i class="fas fa-circle text-success"></i></span>"</span></strong>. Terima kasih.</p>
    </div>

    <div class="row">
      <div class="col-md-4 order-md-2 mb-4">
        <h4 class="d-flex justify-content-between align-items-center mb-3">
          <h4>Daftar Rekening Yang tersedia</h4>
          {{-- <span class="badge badge-secondary badge-pill"></span> --}}
        </h4>
        <ul class="list-group mb-3">
            @forelse ($banks as $bank)
            @if ($bank->is_active == 1)
                <li class="list-group-item d-flex justify-content-between lh-condensed">
                    <div>
                        <h6 class="my-0">{{ $bank->nama_bank }} a/n {{ $bank->nama_pemilik }}</h6>
                        <small class="text-muted">{{ $bank->no_rekening }}</small>
                    </div>
                    <span class="text-success">Aktif <i class="fas fa-circle"></i></span>
                </li>
            @else
                <li class="list-group-item d-flex justify-content-between lh-condensed">
                    <div>
                        <h6 class="my-0 text-muted">{{ $bank->nama_bank }} a/n {{ $bank->nama_pemilik }}</h6>
                        <small class="text-muted">{{ $bank->no_rekening }}</small>
                    </div>
                    <span class="text-danger">Tidak Aktif <i class="fas fa-circle"></i></span>
                </li>
            @endif
            @empty
                <li class="list-group-item d-flex justify-content-between lh-condensed">
                    <hg class="my-0">Belum ada rekening yang tersedia.</hg>
                </li>
            @endforelse
        </ul>
      </div>
      <div class="col-md-8 order-md-1">
        <h4 class="mb-3">Cara pembayaran via ATM Transfer:</h4>
        <ol>
            <li>Masukkan kartu ATM kemudian masukkan nomor PIN Anda</li>
            <li>Pilih "Transaksi lainnya", kemudian pilih "Transfer"</li>
            <li>Silahkan masukkan no. rekening yang aktif<b>(pilih salah satu)</b>, lalu tekan "Benar"</li>
            <li>Masukkan nominal jumlah pembayaran</li>
            <li>Periksa kembali data transaksi kemudian tekan "Benar"</li>
            <li>Simpan struk Anda sebagai bukti pembayaran. Penjual akan menerima notifikasi pembayaran.</li>
        </ol>
        <hr>
        <h4 class="mb-3">Apabila sudah melakukan pembayaran, silahkan konfirmasi pembayaran <a href="{{ route('user.konfirmasi') }}" target="_blank">disini</a>.</h4>
      </div>
    </div>

    <footer class="my-5 pt-5 text-muted text-center text-small">
      <p class="mb-1">&copy; 2017-2018 Company Name</p>
      <ul class="list-inline">
        <li class="list-inline-item"><a href="#">Privacy</a></li>
        <li class="list-inline-item"><a href="#">Terms</a></li>
        <li class="list-inline-item"><a href="#">Support</a></li>
      </ul>
    </footer>
  </div>
@endsection