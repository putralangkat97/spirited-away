<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Olimpiade extends Model
{
    use \Staudenmeir\EloquentHasManyDeep\HasRelationships;

    protected $guarded = [];

    public function users() {
        return $this->belongsToMany('App\User');
    }

    public function soals() {
        return $this->hasMany('App\Models\Quiz', 'quiz_id');
    }

    public function resultOlimpiades() {
        return $this->hasMany('App\Models\Result', 'olimpiade_id');
    }

    public function userlogs() {
        return $this->hasMany('App\Models\UserLog', 'olimpiade_id');
    }
}
