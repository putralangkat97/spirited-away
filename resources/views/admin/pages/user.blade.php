@extends('admin.layouts.master')

@section('title', 'User')

@section('page-title', 'Semua User')

@section('content')
    <div class="row">
        <div class="col-xl-12">
            <div class="card-box table-responsive">
                @if (session()->has('success'))
                <div class="alert alert-success">
                    <strong>Sukses!</strong> {{ session('success') }}
                </div>
                @elseif (session()->has('imported'))
                <div class="alert alert-success">
                    <strong>Sukses!</strong> {{ session('imported') }}
                </div>
                @endif
                <h4 class="header-title mb-2">User</h4>
                <a href="{{ route('admin.userAdd') }}" class="sub-header btn btn-rounded btn-sm btn-primary text-white"><i class="fa fa-plus"></i></a>
                <a href="{{ route('admin.excel') }}" class="btn text-white btn-sm btn-rounded btn-success sub-header" target="_blank"><i class="mdi mdi-file-excel"></i></a>
<a class="btn btn-sm sub-header text-white btn-rounded btn-info" href="{{ route('admin.import') }}"><i class="mdi mdi-upload"></i></a>
                <table id="datatable" class="table table-bordered  dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead class="text-center">
                        <tr>
                            <th>#</th>
                            <th>Nama Lengkap</th>
                            <th>L/P</th>
                            <th>Tgl. Lahir</th>
                            <th>No. Telp</th>
                            <th>Olimpiade</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody class="text-center">
                        @forelse($users as $key => $user)
                        <tr>
                            <td>{{ ++$key }}</td>
                            <td>{{ $user->nama_lengkap }}</td>
                            @if($user->jenis_kelamin == 'Laki-Laki' || $user->jenis_kelamin == 'laki-laki' || $user->jenis_kelamin == 'Laki-laki')
                            <td>L</td>
                            @elseif($user->jenis_kelamin == 'Perempuan' || $user->jenis_kelamin == 'perempuan')
                            <td>P</td>
                            @endif
                            <td>{{ date('d M Y', strtotime($user->tanggal_lahir)) }}</td>
                            <td>{{ $user->no_telepon_whatsapp }}</td>
                            <td>{{ implode(', ', $user->olimpiades()->get()->pluck('nama_olimpiade')->toArray()) }}</td>
                            @if($user->is_active == 0)
                            <td><span class="badge badge-pill badge-danger">Inactive</span></td>
                            @else
                            <td><span class="badge badge-pill badge-success">Active</span></td>
                            @endif
                            <td>
                                @if($user->is_active == 0)
                                <a href="#" class="btn btn-rounded btn-success" title="Aktifkan User" onclick="active({{ $user->id }})"><i class="mdi mdi-power"></i></a>
                                @else
                                <a href="#" class="btn btn-rounded btn-warning" title="Suspend User" onclick="inactive({{ $user->id }})"><i class="mdi mdi-power"></i></a>
                                @endif
                                &nbsp;
                                <a href="#" class="detail btn btn-rounded btn-info" title="Detail User" id="{{ $user->id }}"><i class="mdi mdi-eye"></i></a>
                                &nbsp;
                                <a href="#" class="btn btn-rounded btn-danger" onclick="deleteConfirmation({{ $user->id }})" title="Hapus User"><i class="mdi mdi-trash-can"></i></a>
                            </td>
                        </tr>
                        @empty
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@include('admin.modals._user')
@endsection

@section('script')
<script src="{{ asset('assets/admin/js/user.js') }}"></script>
@endsection
