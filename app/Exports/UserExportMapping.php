<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Reader\Xls\Style\Border;

class UserExportMapping implements FromCollection, WithMapping, WithHeadings, ShouldAutoSize, WithEvents {
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection() {
      return User::with('olimpiades')->with('province')->with('kecamatan')->get();
    }

    public function map($users) : array {
      return [
        $users->nama_lengkap,
        $users->username,
        $users->email,
        date('d F Y', strtotime($users->tanggal_lahir)),
        $users->tempat_lahir,
        $users->jenis_kelamin,
        $users->asal_sekolah,
        $users->kelas,
        implode(', ', $users->olimpiades()->get()->pluck('nama_olimpiade')->toArray()),
        implode($users->olimpiades()->get()->pluck('tingkat')->toArray()),
        implode($users->province()->where('id', '=', $users->provinsi_id)->get()->pluck('name')->toArray()),
        implode($users->kecamatan()->where('id', '=', $users->kabupaten_kota_id)->get()->pluck('name')->toArray()),
        $users->alamat,
        $users->no_telepon_whatsapp,
        $users->jumlah_point
      ];
    }

    public function headings() : array {
      return [
        'Nama Peserta',
        'Username',
        'E-mail',
        'Tanggal Lahir',
        'Tempat Lahir',
        'Jenis Kelamin',
        'Asal Sekolah',
        'Kelas',
        'Olimpiade',
        'Tingkat',
        'Provinsi',
        'Kabupaten',
        'Alamat',
        'No. Telp/WA',
        'Jumlah Point'
      ];
    }
    
    public function registerEvents() : array {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $cellRange = 'A1:'.$event->sheet->getDelegate()->getHighestColumn().'1';
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
                
                // Apply array of styles to B2:G8 cell range
                $styleArray = array(
                    'borders' => array(
                        'allBorders' => array(
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['rgb' => '2d2d2d']
                        )
                    ),
                    'alignment' => array(
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                    ),
                );
                
                $col = $event->sheet->getDelegate()->getHighestColumn();
                $row = $event->sheet->getDelegate()->getHighestRow();
                $event->sheet->getDelegate()->getStyle('A1:'.$col.$row)->applyFromArray($styleArray);

                // Set first row to height 20
                 $event->sheet->getDelegate()->getRowDimension(1)->setRowHeight(26);
            },
        ];
    }
}
