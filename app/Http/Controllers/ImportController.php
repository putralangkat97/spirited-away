<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\UserImport;
use Maatwebsite\Excel\Facades\Excel;

class ImportController extends Controller
{
    public function __construct() {
        $this->middleware('auth:admin');
    }

    public function userImport() {
        return view('admin.pages.userImport');
    }

    public function importGuide() {
        return view('admin.pages.userGuide');
    }

    public function importStore(Request $request) {
        $file = $request->file('import')->store('import');
        $import = new UserImport;
        $import->import($file);

        return redirect()->route('admin.user')->with('imported', 'User berhasil diimport');
    }
}
