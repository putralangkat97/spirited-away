<meta name="csrf-token" content="{{ csrf_token() }}"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!-- App favicon -->
<link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}">

<!-- C3 Chart css -->
<link href="{{ asset('assets/libs/c3/c3.min.css') }}" rel="stylesheet" type="text/css" />
<!-- third party css -->
<link href="{{ asset('assets/libs/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/datatables/buttons.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/datatables/responsive.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
<!-- App css -->
<link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" id="bootstrap-stylesheet" />
<link href="{{ asset('assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/app.min.css') }}" rel="stylesheet" type="text/css"  id="app-stylesheet" />
<link href="{{ asset('css/sweetalert2.min.css') }}" rel="stylesheet" type="text/css"  id="app-stylesheet" />
<link href="{{asset('css/select2.min.css')}}" rel="stylesheet" type="text/css" id="app-stylesheet" />
<link href="{{asset('css/gijgo.min.css')}}" rel="stylesheet" type="text/css" id="app-stylesheet" />
<link rel="stylesheet" href="{{ asset('css/bootstrap-datetimepickers.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/parsley.css') }}">
