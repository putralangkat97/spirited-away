$(document).ready(function () {
    $('#datatable').DataTable()

    $('#lihat').click(function () {
        var id = $(this).attr('data-id')
        $.ajax({
            url: '/admin/pembayaran/bukti/'+id,
            dataType: 'JSON',
            success: function (data) {
                console.log(data)
                $('#bukti').attr('src', '/bukti/'+data.bukti.image)
                $('#modalBukti').modal('show')
            }
        })
    })
})

function confirm(id) {
    Swal.fire({
        icon: 'warning',
        title: "Yang bener..?",
        text: "Anda yakin ingin mengkonfirmasikan pembayaran ini?",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Aktifkan",
        cancelButtonText: "Batal",
        reverseButtons: !0
    }).then(function (event) {
        if (event.value === true) {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                type: 'POST',
                url: '/admin/pembayaran/confirm/'+id,
                data: { _token: CSRF_TOKEN },
                dataType: 'JSON',
                success: function (results) {
                    if (results.success === true) {
                        Swal.fire({
                            // toast: true,
                            icon: 'success',
                            title: "Success",
                            text: "Pembayaran telah dikonfirmasi",
                            showConfirmButton: true,
                            timer: 1500
                        }).then(function() {
                            location.reload();
                        });
                    } else {
                        Swal.fire("Error!", results.message, "error");
                    }
                }
            });

        } else {
            e.dismiss;
        }

    }, function (dismiss) {
        return false;
    })
}

function batal(id) {
    Swal.fire({
        icon: 'warning',
        title: "Yang bener..?",
        text: "Anda yakin ingin membatalkan pembayaran ini?",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Aktifkan",
        cancelButtonText: "Batal",
        reverseButtons: !0
    }).then(function (event) {
        if (event.value === true) {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                type: 'POST',
                url: '/admin/pembayaran/batal/'+id,
                data: { _token: CSRF_TOKEN },
                dataType: 'JSON',
                success: function (results) {
                    if (results.success === true) {
                        Swal.fire({
                            // toast: true,
                            icon: 'success',
                            title: "Success",
                            text: "Pembayaran telah dibatalkan",
                            showConfirmButton: true,
                            timer: 1500
                        }).then(function() {
                            location.reload();
                        });
                    } else {
                        Swal.fire("Error!", results.message, "error");
                    }
                }
            });

        } else {
            e.dismiss;
        }

    }, function (dismiss) {
        return false;
    })
}

function aktifkan() {
    window.open("/admin/user", "_blank")
}