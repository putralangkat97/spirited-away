@extends('admin.layouts.master')

@section('title', 'Pembayaran')

@section('title-page', 'Pembayaran')

@section('content')
    <div class="row">
        <div class="col-xl-12">
            <div class="card-box table-responsive">
                <h4 class="header-title mb-2">Pembayaran</h4>
                <table id="datatable" class="table table-bordered  dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead class="text-center">
                        <tr>
                            <th>#</th>
                            <th>No. Pembayaran</th>
                            <th>Pengirim</th>
                            <th>Tgl. Transfer</th>
                            <th>Rek. Tujuan</th>
                            <th>Nominal</th>
                            <th>Bukti Pembayaran</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody class="text-center">
                        @php
                            $no = 0;
                        @endphp
                        @foreach ($datas as $p)
                        @php
                            $no++;
                        @endphp
                        <tr>
                            <td>{{ $no }}</td>
                            <td>{{ $p->no_pembayaran }}</td>
                            <td>{{ $p->user->nama_lengkap }}</td>
                            <td>{{ date('d-m-Y', strtotime($p->tanggal_transfer)) }}</td>
                            <td>{{ $p->bank->nama_bank }} ({{$p->bank->no_rekening}}/{{ $p->pemilik_rekening }})</td>
                            <td>Rp. {{ number_format($p->nominal) }}</td>
                            <td>
                                <button id="lihat" data-id="{{ $p->id }}" class="btn btn-sm btn-primary">Lihat <i class="mdi mdi-eye"></i></button>
                            </td>
                            <td><span class="badge {{ $p->status == 0 ? 'badge-danger' : 'badge-success' }}">{{ $p->status == 0 ? 'Belum dikonfirmasi' : 'Selesai' }}</span></td>
                            @if ($p->status == 0)
                                <td>
                                    <button onclick="confirm({{ $p->id }})" class="konfirm btn btn-rounded btn-warning" title="Konfirmasi"><i class="mdi mdi-check-bold"></i></button>
                                </td>
                            @else
                                <td>
                                    <button onclick="batal({{ $p->id }})" class="batal btn btn-rounded btn-danger" title="Batalkan Pembayaran"><i class="mdi mdi-close"></i></button>
                                    &nbsp;
                                    @if ($p->user->is_active == 1)
                                    <button class="btn btn-rounded btn-success" title="User Telah Aktif" disabled><i class="mdi mdi-check-bold"></i></button>
                                    @else
                                    <button onclick="aktifkan();" class="btn btn-rounded btn-outline-warning" title="Aktifkan User"><i class="mdi mdi-power"></i></button>
                                    @endif
                                </td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@include('admin.modals._bukti')
@endsection

@section('script')
<script src="{{ asset('assets/admin/js/pembayaran.js') }}"></script>
@endsection
