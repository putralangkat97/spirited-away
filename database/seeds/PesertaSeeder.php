<?php

use App\User;
use Illuminate\Database\Seeder;

class PesertaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        User::create([
            'nama_lengkap'  => 'Peserta 1',
            'username'      => 'peserta1',
            'tanggal_lahir' => date('Y-m-d', strtotime(03-02-1997)),
            'tempat_lahir' => 'Medan',
            'jenis_kelamin' => 'Perempuan',
            'alamat' => 'Medan',
            'no_telepon_whatsapp' => '082288763734',
            'provinsi_id' => 12,
            'kabupaten_kota_id' => 1213,
            'asal_sekolah' => 'SMA N 1 Medan',
            'kelas' => 11,
            'email' => 'peserta1@gmail.com',
            'password' => bcrypt('peserta1'),
            'jumlah_point' => 0,
            'is_active' => 0,
        ]);
        
        User::create([
            'nama_lengkap'  => 'Peserta 2',
            'username'      => 'peserta2',
            'tanggal_lahir' => date('Y-m-d', strtotime(03-02-2997)),
            'tempat_lahir' => 'Medan',
            'jenis_kelamin' => 'Laki-Laki',
            'alamat' => 'Medan',
            'no_telepon_whatsapp' => '082288763734',
            'provinsi_id' => 12,
            'kabupaten_kota_id' => 1213,
            'asal_sekolah' => 'SMA N 1 Medan',
            'kelas' => 11,
            'email' => 'peserta2@gmail.com',
            'password' => bcrypt('peserta2'),
            'jumlah_point' => 0,
            'is_active' => 0,
        ]);
    }
}
