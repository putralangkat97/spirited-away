<!DOCTYPE html>
<html lang="en" dir="ltr">
<!-- Required meta tags -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>POSI</title>
<!-- plugins:css -->
<link rel="stylesheet" href="{{ asset('css/iconfonts/mdi/css/materialdesignicons.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/iconfonts/flag-icon-css/css/flag-icon.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/iconfonts/simple-line-icon/css/simple-line-icons.css') }}">
<link rel="stylesheet" href="{{ asset('css/iconfonts/ti-icons/css/themify-icons.css') }}">
<link rel="stylesheet" href="{{ asset('css/iconfonts/font-awesome/css/font-awesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/vendor.bundle.base.css') }}">
<link rel="stylesheet" href="{{ asset('css/vendor.bundle.addons.css') }}">
<link rel="stylesheet" href="{{ asset('css/sweetalert2.css') }}">
<link href="{{ asset('assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
<link href="https://fonts.googleapis.com/css?family=Titillium+Web&display=swap" rel="stylesheet">
<!-- endinject -->
<!-- plugin css for this page -->
<!-- End plugin css for this page -->
<!-- inject:css -->
<link rel="stylesheet" href="{{ asset('css/style.css') }}">
<!-- endinject -->
<link rel="shortcut icon" href="{{ asset('images/favicon.png') }}" />
<link rel="stylesheet" href="{{ asset('css/gijgo.min.css') }}">

<body>
<div class="container-scroller" style="font-family: 'Titillium Web', sans-serif;">
<div class="container-fluid page-body-wrapper full-page-wrapper">
  <div class="content-wrapper auth p-0 theme-two">
    <div class="row d-flex align-items-stretch">
      <div class="col-md-4 banner-section d-none d-md-flex align-items-stretch justify-content-center">
        <div class="slide-content bg-1">
        </div>
      </div>
        <div class="col-12 col-md-8 h-100 bg-white">
          <div class="auto-form-wrapper d-flex align-items-center justify-content-center flex-column">
            <div class="uk-text-center uk-margin-bottom">
              <div class="row">
                <div class="col-md-12">
                  <a class="uk-logo uk-text-success uk-text-bold" href="/"><img src="{{ asset('img/posi.png') }}" style="max-width: 230px;"></a>
                </div>
              </div>
            </div>
            <br>
            <br>
            <form method="POST" action="{{ route('login') }}">
               @csrf
                      <div class="form-group">
                          <div class="input-group">
                              <div class="input-group-prepend">
                                  <span class="input-group-text"><i class="mdi mdi-account"></i></span>
                              </div>
                              <input id="email" type="email" class="form-control rounded @error('email') is-invalid @enderror" name="email"value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Email">

                          </div>
                      </div>

                      <div class="form-group">
                      <div class="input-group">
                          <div class="input-group-prepend">
                              <span class="input-group-text"><i class="mdi mdi-lock"></i></span>
                              </div>
                          <input id="password" type="password" class="form-control  @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">
                              @error('password')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                              @enderror
                          </div>
                      </div>
                      <div class="form-group row">
                          <div class="col-md-4 offset  ">
                              <button type="submit" class="btn btn-block btn-primary rounded" >
                                  {{ __('Log In') }}
                              </button>
                            <!--  forget -->
                          </div>
                      </div>
                  <div class="wrapper mt-5 text-gray">
                    <p class="footer-text">Pelatihan Olimpiade Sains Indonesia © 2020<br>Design by <a href="https://n56ht.com" target="_blank">InSight MarComm</a></p>
                  </div>
              </form>
          </div>
        </div>
      </div>
    </div>
    <!-- content-wrapper ends -->
  </div>
  <!-- page-body-wrapper ends -->
</div>
<!-- plugins:js -->
<script src="{{ asset('js/jquery.js') }}"></script>
</body>

<!-- container-scroller -->

</html>
