<div id="modalPenilaian" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Penilaian</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form id="formPenilaian" method="post" data-parsley-validate>
                @csrf
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="nama_penilaian">Nama Penilaian:</label>
                                <input type="text" name="nama_penilaian" id="nama_penilaian" class="form-control" required placeholder="Nama Penilaian" data-parsley-required-message="Nama penilaian tidak boleh kosong.">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="level">Level:</label>
                                <select name="level" id="level" class="form-control" required>
                                    <option value="">Pilih</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <h4>Benar</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="pointA">Point Awal:</label>
                                <input type="text" oninput="pekalian()" name="pointA" id="pointA" class="form-control" required data-parsley-required-message="Point awal harus diisi">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="perkalian">Dikalikan:</label>
                                <input type="text" oninput="pekalian()" name="perkalian" id="perkalian" class="form-control" required data-parsley-required-message="Perkaliann harus diisi.">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="benar">Point Benar</label>
                                <input type="text" name="benar" id="benar" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col">
                            <h4>Salah</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="pointB">Point Awal:</label>
                                <input type="text" oninput="pekalianB()" name="pointB" id="pointB" class="form-control" required data-parsley-required-message="Point awal tidak boleh kosong.">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="perkalianB">Dikalikan:</label>
                                <input type="text" oninput="pekalianB()" name="perkalianB" id="perkalianB" class="form-control" required data-parsley-required-message="Perkalian tidak boleh kosong.">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="salah">Point Salah</label>
                                <input type="text" name="salah" id="salah" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <input type="hidden" name="hidden_id" id="id">
                            <input type="hidden" name="add" id="tambah" value="Tambah">
                            <input class="btn btn-primary btn-block" type="submit" id="action" value="tambah">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>