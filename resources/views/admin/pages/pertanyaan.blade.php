@extends('admin.layouts.master')

@section('title', 'Pertanyaan')

@section('page-title', 'Buat Pertanyaan')

@section('content')
    <div class="row">
        <div class="col-xl-12">
            <div class="card-box">
                <h4 class="header-title mb-2">Kategori: <strong>{{ $quizzes->olimpiade->nama_olimpiade }} {{ $quizzes->olimpiade->tingkat }} ({{ $quizzes->kode_soal }})</strong></h4>
                <hr>
                @if(session()->has('message'))
                    <div class="alert alert-success text-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>Sukses! </strong>{{ session()->get('message') }}
                    </div>
                @elseif(session()->has('hapus'))
                    <div class="alert alert-danger text-danger alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>Sukses! </strong>{{ session()->get('hapus') }}
                    </div>
                @endif
                <h4 class="header-title mt-2">Tulis Pertanyaan:</h4>
                <form action="{{ route('admin.pertanyaanAdd') }}" method="post" data-parsley-validate>
                @csrf
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <textarea name="pertanyaan" id="summernote" class="form-control" required data-parsley-required-message="Pertanyaan tidak boleh kosong."></textarea>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="quiz_id" value="{{ $quizzes->id }}">
                    <input type="submit" value="Submit" class="btn btn-rounded btn-primary">
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="card-box table-responsive">
                <h4 class="header-title">Daftar Pertanyaan</h4>
                <table id="datatable" class="table table-bordered  dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Soal</th>
                            <th class="text-center">Jumlah Jawaban</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($quizzes ->pertanyaans as $key => $pertanyaan)
                        <tr>
                            <td>{{ ++$key }}</td>
                            <td>{!! $pertanyaan->pertanyaan !!}</td>
                            <td class="text-center">{{ $pertanyaan->jawabans->count() }}</td>
                            <td class="text-center">
                                <form action="{{ route('admin.pertanyaanDelete', $pertanyaan->id) }}" method="POST">
                                    <a href="{{ route('admin.jawaban', $pertanyaan->id) }}" class="btn btn-rounded btn-warning" title="Buat jawaban"><i class="mdi mdi-pencil-plus"></i></a>
                                    &nbsp;
                                    <a href="{{ route('admin.pertanyaanEdit', $pertanyaan->id) }}" class="btn btn-rounded btn-info" title="Edit"><i class="mdi mdi-pencil"></i></a>
                                    &nbsp;
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-rounded btn-danger" title="Hapus"><i class="mdi mdi-trash-can"></i></button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
<script src="{{ asset('assets/admin/js/pertanyaan.js') }}"></script>
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script>
    CKEDITOR.replace('summernote', {
        filebrowserImageBrowseUrl: '/filemanager?type=Images',
        filebrowserImageUploadUrl: '/filemanager/upload?type=Images&_token=',
        filebrowserBrowseUrl: '/filemanager?type=Files',
        filebrowserUploadUrl: '/filemanager/upload?type=Files&_token='
    });
</script>
@endsection