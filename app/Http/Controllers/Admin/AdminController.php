<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Admin;
use Carbon\Carbon;
use App\Models\Bank;
use App\Models\Quiz;
use App\Models\Jawaban;
use App\Models\Province;
use Carbon\CarbonPeriod;
use App\Models\Kecamatan;
use App\Models\Olimpiade;
use App\Models\Penilaian;
use App\Models\Pembayaran;
use App\Models\Pertanyaan;
use Illuminate\Http\Request;
use App\Exports\UserExportMapping;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Intervention\Image\ImageManagerStatic as Image;

class AdminController extends Controller
{
    public function __construct() {
        $this->middleware('auth:admin');
    }

    public function index() {
        $year = Carbon::now()->format('Y');
        $jan = Carbon::create($year.'-01-01')->format('m');
        $feb = Carbon::create($year.'-02-01')->format('m');
        $mar = Carbon::create($year.'-03-01')->format('m');
        $apr = Carbon::create($year.'-04-01')->format('m');
        $mei = Carbon::create($year.'-05-01')->format('m');
        $jun = Carbon::create($year.'-06-01')->format('m');
        $jul = Carbon::create($year.'-07-01')->format('m');
        $agu = Carbon::create($year.'-08-01')->format('m');
        $sep = Carbon::create($year.'-09-01')->format('m');
        $okt = Carbon::create($year.'-10-01')->format('m');
        $nov = Carbon::create($year.'-11-01')->format('m');
        $des = Carbon::create($year.'-12-01')->format('m');

        $datajanSD1 = User::whereMonth('created_at', '=', $jan)->where('kelas', 1)->count();
        $datajanSD2 = User::whereMonth('created_at', '=', $jan)->where('kelas', 2)->count();
        $datajanSD3 = User::whereMonth('created_at', '=', $jan)->where('kelas', 3)->count();
        $datajanSD4 = User::whereMonth('created_at', '=', $jan)->where('kelas', 4)->count();
        $datajanSD5 = User::whereMonth('created_at', '=', $jan)->where('kelas', 5)->count();
        $datajanSD6 = User::whereMonth('created_at', '=', $jan)->where('kelas', 6)->count();
        $datajanSMP1 = User::whereMonth('created_at', '=', $jan)->where('kelas', 7)->count();
        $datajanSMP2 = User::whereMonth('created_at', '=', $jan)->where('kelas', 8)->count();
        $datajanSMP3 = User::whereMonth('created_at', '=', $jan)->where('kelas', 9)->count();
        $datajanSMA1 = User::whereMonth('created_at', '=', $jan)->where('kelas', 10)->count();
        $datajanSMA2 = User::whereMonth('created_at', '=', $jan)->where('kelas', 11)->count();
        $datajanSMA3 = User::whereMonth('created_at', '=', $jan)->where('kelas', 12)->count();
        $datajanSD = $datajanSD1 + $datajanSD2 + $datajanSD3 + $datajanSD4 + $datajanSD5 + $datajanSD6;
        $datajanSMP = $datajanSMP1 + $datajanSMP2 + $datajanSMP3;
        $datajanSMA = $datajanSMA1 + $datajanSMA2 + $datajanSMA3;

        $datafebSD1 = User::whereMonth('created_at', '=', $feb)->where('kelas', 1)->count();
        $datafebSD2 = User::whereMonth('created_at', '=', $feb)->where('kelas', 2)->count();
        $datafebSD3 = User::whereMonth('created_at', '=', $feb)->where('kelas', 3)->count();
        $datafebSD4 = User::whereMonth('created_at', '=', $feb)->where('kelas', 4)->count();
        $datafebSD5 = User::whereMonth('created_at', '=', $feb)->where('kelas', 5)->count();
        $datafebSD6 = User::whereMonth('created_at', '=', $feb)->where('kelas', 6)->count();
        $datafebSMP1 = User::whereMonth('created_at', '=', $feb)->where('kelas', 7)->count();
        $datafebSMP2 = User::whereMonth('created_at', '=', $feb)->where('kelas', 8)->count();
        $datafebSMP3 = User::whereMonth('created_at', '=', $feb)->where('kelas', 9)->count();
        $datafebSMA1 = User::whereMonth('created_at', '=', $feb)->where('kelas', 10)->count();
        $datafebSMA2 = User::whereMonth('created_at', '=', $feb)->where('kelas', 11)->count();
        $datafebSMA3 = User::whereMonth('created_at', '=', $feb)->where('kelas', 12)->count();
        $datafebSD = $datafebSD1 + $datafebSD2 + $datafebSD3 + $datafebSD4 + $datafebSD5 + $datafebSD6;
        $datafebSMP = $datafebSMP1 + $datafebSMP2 + $datafebSMP3;
        $datafebSMA = $datafebSMA1 + $datafebSMA2 + $datafebSMA3;

        $datamarSD1 = User::whereMonth('created_at', '=', $mar)->where('kelas', 1)->count();
        $datamarSD2 = User::whereMonth('created_at', '=', $mar)->where('kelas', 2)->count();
        $datamarSD3 = User::whereMonth('created_at', '=', $mar)->where('kelas', 3)->count();
        $datamarSD4 = User::whereMonth('created_at', '=', $mar)->where('kelas', 4)->count();
        $datamarSD5 = User::whereMonth('created_at', '=', $mar)->where('kelas', 5)->count();
        $datamarSD6 = User::whereMonth('created_at', '=', $mar)->where('kelas', 6)->count();
        $datamarSMP1 = User::whereMonth('created_at', '=', $mar)->where('kelas', 7)->count();
        $datamarSMP2 = User::whereMonth('created_at', '=', $mar)->where('kelas', 8)->count();
        $datamarSMP3 = User::whereMonth('created_at', '=', $mar)->where('kelas', 9)->count();
        $datamarSMA1 = User::whereMonth('created_at', '=', $mar)->where('kelas', 10)->count();
        $datamarSMA2 = User::whereMonth('created_at', '=', $mar)->where('kelas', 11)->count();
        $datamarSMA3 = User::whereMonth('created_at', '=', $mar)->where('kelas', 12)->count();
        $datamarSD = $datamarSD1 + $datamarSD2 + $datamarSD3 + $datamarSD4 + $datamarSD5 + $datamarSD6;
        $datamarSMP = $datamarSMP1 + $datamarSMP2 + $datamarSMP3;
        $datamarSMA = $datamarSMA1 + $datamarSMA2 + $datamarSMA3;

        $dataaprSD1 = User::whereMonth('created_at', '=', $apr)->where('kelas', 1)->count();
        $dataaprSD2 = User::whereMonth('created_at', '=', $apr)->where('kelas', 2)->count();
        $dataaprSD3 = User::whereMonth('created_at', '=', $apr)->where('kelas', 3)->count();
        $dataaprSD4 = User::whereMonth('created_at', '=', $apr)->where('kelas', 4)->count();
        $dataaprSD5 = User::whereMonth('created_at', '=', $apr)->where('kelas', 5)->count();
        $dataaprSD6 = User::whereMonth('created_at', '=', $apr)->where('kelas', 6)->count();
        $dataaprSMP1 = User::whereMonth('created_at', '=', $apr)->where('kelas', 7)->count();
        $dataaprSMP2 = User::whereMonth('created_at', '=', $apr)->where('kelas', 8)->count();
        $dataaprSMP3 = User::whereMonth('created_at', '=', $apr)->where('kelas', 9)->count();
        $dataaprSMA1 = User::whereMonth('created_at', '=', $apr)->where('kelas', 10)->count();
        $dataaprSMA2 = User::whereMonth('created_at', '=', $apr)->where('kelas', 11)->count();
        $dataaprSMA3 = User::whereMonth('created_at', '=', $apr)->where('kelas', 12)->count();
        $dataaprSD = $dataaprSD1 + $dataaprSD2 + $dataaprSD3 + $dataaprSD4 + $dataaprSD5 + $dataaprSD6;
        $dataaprSMP = $dataaprSMP1 + $dataaprSMP2 + $dataaprSMP3;
        $dataaprSMA = $dataaprSMA1 + $dataaprSMA2 + $dataaprSMA3;

        $datameiSD1 = User::whereMonth('created_at', '=', $mei)->where('kelas', 1)->count();
        $datameiSD2 = User::whereMonth('created_at', '=', $mei)->where('kelas', 2)->count();
        $datameiSD3 = User::whereMonth('created_at', '=', $mei)->where('kelas', 3)->count();
        $datameiSD4 = User::whereMonth('created_at', '=', $mei)->where('kelas', 4)->count();
        $datameiSD5 = User::whereMonth('created_at', '=', $mei)->where('kelas', 5)->count();
        $datameiSD6 = User::whereMonth('created_at', '=', $mei)->where('kelas', 6)->count();
        $datameiSMP1 = User::whereMonth('created_at', '=', $mei)->where('kelas', 7)->count();
        $datameiSMP2 = User::whereMonth('created_at', '=', $mei)->where('kelas', 8)->count();
        $datameiSMP3 = User::whereMonth('created_at', '=', $mei)->where('kelas', 9)->count();
        $datameiSMA1 = User::whereMonth('created_at', '=', $mei)->where('kelas', 10)->count();
        $datameiSMA2 = User::whereMonth('created_at', '=', $mei)->where('kelas', 11)->count();
        $datameiSMA3 = User::whereMonth('created_at', '=', $mei)->where('kelas', 12)->count();
        $datameiSD = $datameiSD1 + $datameiSD2 + $datameiSD3 + $datameiSD4 + $datameiSD5 + $datameiSD6;
        $datameiSMP = $datameiSMP1 + $datameiSMP2 + $datameiSMP3;
        $datameiSMA = $datameiSMA1 + $datameiSMA2 + $datameiSMA3;

        $datajunSD1 = User::whereMonth('created_at', '=', $jun)->where('kelas', 1)->count();
        $datajunSD2 = User::whereMonth('created_at', '=', $jun)->where('kelas', 2)->count();
        $datajunSD3 = User::whereMonth('created_at', '=', $jun)->where('kelas', 3)->count();
        $datajunSD4 = User::whereMonth('created_at', '=', $jun)->where('kelas', 4)->count();
        $datajunSD5 = User::whereMonth('created_at', '=', $jun)->where('kelas', 5)->count();
        $datajunSD6 = User::whereMonth('created_at', '=', $jun)->where('kelas', 6)->count();
        $datajunSMP1 = User::whereMonth('created_at', '=', $jun)->where('kelas', 7)->count();
        $datajunSMP2 = User::whereMonth('created_at', '=', $jun)->where('kelas', 8)->count();
        $datajunSMP3 = User::whereMonth('created_at', '=', $jun)->where('kelas', 9)->count();
        $datajunSMA1 = User::whereMonth('created_at', '=', $jun)->where('kelas', 10)->count();
        $datajunSMA2 = User::whereMonth('created_at', '=', $jun)->where('kelas', 11)->count();
        $datajunSMA3 = User::whereMonth('created_at', '=', $jun)->where('kelas', 12)->count();
        $datajunSD = $datajunSD1 + $datajunSD2 + $datajunSD3 + $datajunSD4 + $datajunSD5 + $datajunSD6;
        $datajunSMP = $datajunSMP1 + $datajunSMP2 + $datajunSMP3;
        $datajunSMA = $datajunSMA1 + $datajunSMA2 + $datajunSMA3;

        $datajulSD1 = User::whereMonth('created_at', '=', $jul)->where('kelas', 1)->count();
        $datajulSD2 = User::whereMonth('created_at', '=', $jul)->where('kelas', 2)->count();
        $datajulSD3 = User::whereMonth('created_at', '=', $jul)->where('kelas', 3)->count();
        $datajulSD4 = User::whereMonth('created_at', '=', $jul)->where('kelas', 4)->count();
        $datajulSD5 = User::whereMonth('created_at', '=', $jul)->where('kelas', 5)->count();
        $datajulSD6 = User::whereMonth('created_at', '=', $jul)->where('kelas', 6)->count();
        $datajulSMP1 = User::whereMonth('created_at', '=', $jul)->where('kelas', 7)->count();
        $datajulSMP2 = User::whereMonth('created_at', '=', $jul)->where('kelas', 8)->count();
        $datajulSMP3 = User::whereMonth('created_at', '=', $jul)->where('kelas', 9)->count();
        $datajulSMA1 = User::whereMonth('created_at', '=', $jul)->where('kelas', 10)->count();
        $datajulSMA2 = User::whereMonth('created_at', '=', $jul)->where('kelas', 11)->count();
        $datajulSMA3 = User::whereMonth('created_at', '=', $jul)->where('kelas', 12)->count();
        $datajulSD = $datajulSD1 + $datajulSD2 + $datajulSD3 + $datajulSD4 + $datajulSD5 + $datajulSD6;
        $datajulSMP = $datajulSMP1 + $datajulSMP2 + $datajulSMP3;
        $datajulSMA = $datajulSMA1 + $datajulSMA2 + $datajulSMA3;

        $dataaguSD1 = User::whereMonth('created_at', '=', $agu)->where('kelas', 1)->count();
        $dataaguSD2 = User::whereMonth('created_at', '=', $agu)->where('kelas', 2)->count();
        $dataaguSD3 = User::whereMonth('created_at', '=', $agu)->where('kelas', 3)->count();
        $dataaguSD4 = User::whereMonth('created_at', '=', $agu)->where('kelas', 4)->count();
        $dataaguSD5 = User::whereMonth('created_at', '=', $agu)->where('kelas', 5)->count();
        $dataaguSD6 = User::whereMonth('created_at', '=', $agu)->where('kelas', 6)->count();
        $dataaguSMP1 = User::whereMonth('created_at', '=', $agu)->where('kelas', 7)->count();
        $dataaguSMP2 = User::whereMonth('created_at', '=', $agu)->where('kelas', 8)->count();
        $dataaguSMP3 = User::whereMonth('created_at', '=', $agu)->where('kelas', 9)->count();
        $dataaguSMA1 = User::whereMonth('created_at', '=', $agu)->where('kelas', 10)->count();
        $dataaguSMA2 = User::whereMonth('created_at', '=', $agu)->where('kelas', 11)->count();
        $dataaguSMA3 = User::whereMonth('created_at', '=', $agu)->where('kelas', 12)->count();
        $dataaguSD = $dataaguSD1 + $dataaguSD2 + $dataaguSD3 + $dataaguSD4 + $dataaguSD5 + $dataaguSD6;
        $dataaguSMP = $dataaguSMP1 + $dataaguSMP2 + $dataaguSMP3;
        $dataaguSMA = $dataaguSMA1 + $dataaguSMA2 + $dataaguSMA3;

        $datasepSD1 = User::whereMonth('created_at', '=', $sep)->where('kelas', 1)->count();
        $datasepSD2 = User::whereMonth('created_at', '=', $sep)->where('kelas', 2)->count();
        $datasepSD3 = User::whereMonth('created_at', '=', $sep)->where('kelas', 3)->count();
        $datasepSD4 = User::whereMonth('created_at', '=', $sep)->where('kelas', 4)->count();
        $datasepSD5 = User::whereMonth('created_at', '=', $sep)->where('kelas', 5)->count();
        $datasepSD6 = User::whereMonth('created_at', '=', $sep)->where('kelas', 6)->count();
        $datasepSMP1 = User::whereMonth('created_at', '=', $sep)->where('kelas', 7)->count();
        $datasepSMP2 = User::whereMonth('created_at', '=', $sep)->where('kelas', 8)->count();
        $datasepSMP3 = User::whereMonth('created_at', '=', $sep)->where('kelas', 9)->count();
        $datasepSMA1 = User::whereMonth('created_at', '=', $sep)->where('kelas', 10)->count();
        $datasepSMA2 = User::whereMonth('created_at', '=', $sep)->where('kelas', 11)->count();
        $datasepSMA3 = User::whereMonth('created_at', '=', $sep)->where('kelas', 12)->count();
        $datasepSD = $datasepSD1 + $datasepSD2 + $datasepSD3 + $datasepSD4 + $datasepSD5 + $datasepSD6;
        $datasepSMP = $datasepSMP1 + $datasepSMP2 + $datasepSMP3;
        $datasepSMA = $datasepSMA1 + $datasepSMA2 + $datasepSMA3;

        $dataoktSD1 = User::whereMonth('created_at', '=', $okt)->where('kelas', 1)->count();
        $dataoktSD2 = User::whereMonth('created_at', '=', $okt)->where('kelas', 2)->count();
        $dataoktSD3 = User::whereMonth('created_at', '=', $okt)->where('kelas', 3)->count();
        $dataoktSD4 = User::whereMonth('created_at', '=', $okt)->where('kelas', 4)->count();
        $dataoktSD5 = User::whereMonth('created_at', '=', $okt)->where('kelas', 5)->count();
        $dataoktSD6 = User::whereMonth('created_at', '=', $okt)->where('kelas', 6)->count();
        $dataoktSMP1 = User::whereMonth('created_at', '=', $okt)->where('kelas', 7)->count();
        $dataoktSMP2 = User::whereMonth('created_at', '=', $okt)->where('kelas', 8)->count();
        $dataoktSMP3 = User::whereMonth('created_at', '=', $okt)->where('kelas', 9)->count();
        $dataoktSMA1 = User::whereMonth('created_at', '=', $okt)->where('kelas', 10)->count();
        $dataoktSMA2 = User::whereMonth('created_at', '=', $okt)->where('kelas', 11)->count();
        $dataoktSMA3 = User::whereMonth('created_at', '=', $okt)->where('kelas', 12)->count();
        $dataoktSD = $dataoktSD1 + $dataoktSD2 + $dataoktSD3 + $dataoktSD4 + $dataoktSD5 + $dataoktSD6;
        $dataoktSMP = $dataoktSMP1 + $dataoktSMP2 + $dataoktSMP3;
        $dataoktSMA = $dataoktSMA1 + $dataoktSMA2 + $dataoktSMA3;

        $datanovSD1 = User::whereMonth('created_at', '=', $nov)->where('kelas', 1)->count();
        $datanovSD2 = User::whereMonth('created_at', '=', $nov)->where('kelas', 2)->count();
        $datanovSD3 = User::whereMonth('created_at', '=', $nov)->where('kelas', 3)->count();
        $datanovSD4 = User::whereMonth('created_at', '=', $nov)->where('kelas', 4)->count();
        $datanovSD5 = User::whereMonth('created_at', '=', $nov)->where('kelas', 5)->count();
        $datanovSD6 = User::whereMonth('created_at', '=', $nov)->where('kelas', 6)->count();
        $datanovSMP1 = User::whereMonth('created_at', '=', $nov)->where('kelas', 7)->count();
        $datanovSMP2 = User::whereMonth('created_at', '=', $nov)->where('kelas', 8)->count();
        $datanovSMP3 = User::whereMonth('created_at', '=', $nov)->where('kelas', 9)->count();
        $datanovSMA1 = User::whereMonth('created_at', '=', $nov)->where('kelas', 10)->count();
        $datanovSMA2 = User::whereMonth('created_at', '=', $nov)->where('kelas', 11)->count();
        $datanovSMA3 = User::whereMonth('created_at', '=', $nov)->where('kelas', 12)->count();
        $datanovSD = $datanovSD1 + $datanovSD2 + $datanovSD3 + $datanovSD4 + $datanovSD5 + $datanovSD6;
        $datanovSMP = $datanovSMP1 + $datanovSMP2 + $datanovSMP3;
        $datanovSMA = $datanovSMA1 + $datanovSMA2 + $datanovSMA3;

        $datadesSD1 = User::whereMonth('created_at', '=', $des)->where('kelas', 1)->count();
        $datadesSD2 = User::whereMonth('created_at', '=', $des)->where('kelas', 2)->count();
        $datadesSD3 = User::whereMonth('created_at', '=', $des)->where('kelas', 3)->count();
        $datadesSD4 = User::whereMonth('created_at', '=', $des)->where('kelas', 4)->count();
        $datadesSD5 = User::whereMonth('created_at', '=', $des)->where('kelas', 5)->count();
        $datadesSD6 = User::whereMonth('created_at', '=', $des)->where('kelas', 6)->count();
        $datadesSMP1 = User::whereMonth('created_at', '=', $des)->where('kelas', 7)->count();
        $datadesSMP2 = User::whereMonth('created_at', '=', $des)->where('kelas', 8)->count();
        $datadesSMP3 = User::whereMonth('created_at', '=', $des)->where('kelas', 9)->count();
        $datadesSMA1 = User::whereMonth('created_at', '=', $des)->where('kelas', 10)->count();
        $datadesSMA2 = User::whereMonth('created_at', '=', $des)->where('kelas', 11)->count();
        $datadesSMA3 = User::whereMonth('created_at', '=', $des)->where('kelas', 12)->count();
        $datadesSD = $datadesSD1 + $datadesSD2 + $datadesSD3 + $datadesSD4 + $datadesSD5 + $datadesSD6;
        $datadesSMP = $datadesSMP1 + $datadesSMP2 + $datadesSMP3;
        $datadesSMA = $datadesSMA1 + $datadesSMA2 + $datadesSMA3;

        $total = User::all()->count();
        $dataLast = User::orderBy('created_at', 'desc')->first();
        $lakilast = User::where('jenis_kelamin', '=', 'Laki-Laki')->orderBy('created_at', 'desc')->first();
        $puanlast = User::where('jenis_kelamin', '=', 'Perempuan')->orderBy('created_at', 'desc')->first();
        $laki = User::where('jenis_kelamin', 'Laki-Laki')->count();
        $puan = User::where('jenis_kelamin', 'Perempuan')->count();

        return view('admin.index', [
            'total' => $total,
            'dataLast' => $dataLast,
            'janSD' => $datajanSD, 'janSMP' => $datajanSMP, 'janSMA' => $datajanSMA,
            'febSD' => $datafebSD, 'febSMP' => $datafebSMP, 'febSMA' => $datafebSMA,
            'marSD' => $datamarSD, 'marSMP' => $datamarSMP, 'marSMA' => $datamarSMA,
            'aprSD' => $dataaprSD, 'aprSMP' => $dataaprSMP, 'aprSMA' => $dataaprSMA,
            'meiSD' => $datameiSD, 'meiSMP' => $datameiSMP, 'meiSMA' => $datameiSMA,
            'junSD' => $datajunSD, 'junSMP' => $datajunSMP, 'junSMA' => $datajunSMA,
            'julSD' => $datajulSD, 'julSMP' => $datajulSMP, 'julSMA' => $datajulSMA,
            'aguSD' => $dataaguSD, 'aguSMP' => $dataaguSMP, 'aguSMA' => $dataaguSMA,
            'sepSD' => $datasepSD, 'sepSMP' => $datasepSMP, 'sepSMA' => $datasepSMA,
            'oktSD' => $dataoktSD, 'oktSMP' => $dataoktSMP, 'oktSMA' => $dataoktSMA,
            'novSD' => $datanovSD, 'novSMP' => $datanovSMP, 'novSMA' => $datanovSMA,
            'desSD' => $datadesSD, 'desSMP' => $datadesSMP, 'desSMA' => $datadesSMA,
            'laki' => $laki,
            'puan' => $puan,
            'lakilast' => $lakilast,
            'puanlast' => $puanlast,
            'users' => User::orderBy('jumlah_point', 'desc')->where('is_active', 1)->get()
        ]);
    }

    // Quiz //
    public function quiz() {
        $olimpiades = Olimpiade::orderBy('nama_olimpiade', 'asc')->get();
        $quizzes = Quiz::orderBy('id', 'desc')->with('olimpiade')->with('pertanyaans')->get();
        return view('admin.pages.quiz')->with(['olimpiades' => $olimpiades, 'quizzes' => $quizzes]);
    }

    public function quizAdd(Request $request) {
        $form = array(
            'olimpiade_id'  => $request->olimpiade_id,
            'waktu'         => $request->waktu,
            'kode_soal'     => $request->kode_soal
        );

        Quiz::create($form);

        return response()->json(['success' => 'Quiz berhasil ditambahkan']);
    }

    public function quizEdit($id) {
        $quizzes = Quiz::findOrFail($id);

        return response()->json(['quiz' => $quizzes]);
    }

    public function quizUpdate(Request $request, Quiz $quiz) {
        $form = array(
            'olimpiade_id'  => $request->olimpiade_id,
            'waktu'         => $request->waktu,
            'kode_soal'     => $request->kode_soal
        );

        Quiz::whereId($request->hidden_id)->update($form);

        return response()->json(['success' => 'Quiz berhasil diupdate']);
    }

    public function quizDelete($id) {
        $data = Quiz::where('id', $id)->delete();
        // check data deleted or not
        if ($data == 1) {
            $success = true;
            $message = "Data deleted successfully";
        } else {
            $success = true;
            $message = "Data not found";
        }
        //  Return response
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }

    // Pertanyaan
    public function pertanyaan($id) {
        $quizzes = Quiz::where('id', $id)->with('olimpiade')->with([
            'pertanyaans' => function ($query) {
                return $query->orderBy('id', 'desc')->with('jawabans');
            }
        ])->get();

        foreach ($quizzes as $value) {
            $value;
        }

        return view('admin.pages.pertanyaan')->with('quizzes', $value);
    }

    public function pertanyaanImage(Request $request) {
        if($request->hasFile('upload')) {
            //get filename with extension
            $filenamewithextension = $request->file('upload')->getClientOriginalName();
       
            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
       
            //get file extension
            $extension = $request->file('upload')->getClientOriginalExtension();
       
            //filename to store
            $filenametostore = $filename.'_'.time().'.'.$extension;
       
            //Upload File
            $request->file('upload')->storeAs('/public/uploads', $filenametostore);
     
            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('/public/uploads/'.$filenametostore); 
            $msg = 'Gambar BERHASIL di upload'; 
            $re = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";
              
            // Render HTML output 
            @header('Content-type: text/html; charset=utf-8'); 
            echo $re;
        }
    }

    public function pertanyaanAdd(Request $request) {
        $this->validate($request, [
            'pertanyaan' => 'required',
            'quiz_id' => 'required',
        ]);

        Pertanyaan::create([
            'pertanyaan'    => $request->pertanyaan,
            'quiz_id'       => $request->quiz_id
        ]);

        return redirect()->back()->with('message', 'Pertanyaan berhasil ditambahkan');
    }

    public function pertanyaanEdit($id) {
        $pertanyaans = Pertanyaan::where('id', $id)->with([
            'quiz' => function ($query) {
                return $query->with([
                    'olimpiade' => function ($query) {
                        return $query;
                    }
                ]);
            }
        ])->get();

        foreach ($pertanyaans as $value) {
            $value;
        }

        return view('admin.pages.pertanyaanEdit')->with('pertanyaans', $value);
    }

    public function pertanyaanUpdate(Request $request) {
        $this->validate($request, [
            'pertanyaan' => 'required',
            'quiz_id' => 'required',
        ]);

        $dd = Pertanyaan::whereId($request->id)->update([
            'pertanyaan'    => $request->pertanyaan,
            'quiz_id'       => $request->quiz_id
        ]);

        return redirect()->route('admin.pertanyaan', $request->quiz_id)->with('message', 'Pertanyaan berhasil diedit');
    }

    public function pertanyaanDelete($id) {
        $pertanyaans = Pertanyaan::findOrFail($id)->delete();

        return redirect()->back()->with('hapus', 'Pertanyaan berhasil dihapus');
    }

    // Jawaban
    public function jawaban($id) {
        $pertanyaans = Pertanyaan::where('id', $id)->with([
            'jawabans' => function ($query) {
                return $query->with([
                    'penilaian' => function ($query) {
                        return $query;
                    }
                ]);
            }
        ])->get();
        $penilaians  = Penilaian::orderBy('nama_penilaian', 'asc')->get();

        foreach ($pertanyaans as $value) {
            $value;
        }

        return view('admin.pages.jawaban')->with(['pertanyaans' => $value, 'penilaians' => $penilaians]);
    }

    public function jawabanAdd(Request $request) {
        $this->validate($request, [
            'jawaban' => 'required',
            'correct' => 'required',
            'penilaian_id' => 'required',
        ]);
        
        $penilaian = Penilaian::where('id', $request->penilaian_id)->select('id', 'benar', 'salah')->get();

        $point = '';
        $jawaban = '';
        if ($request->correct == 'Y') {
            $point = $penilaian->pluck('benar');
            $jawaban = 'Y';
        } else if ($request->correct == 'N') {
            $point = $penilaian->pluck('salah');
            $jawaban = 'N';
        }

        foreach ($point as $key => $value) {
            $form = array(
                'pertanyaan_id'     => $request->pertanyaan_id,
                'jawaban'           => $request->jawaban,
                'correct'           => $jawaban,
                'point'             => $value,
                'penilaian_id'      => $request->penilaian_id,
            );
        }

        Jawaban::create($form);

        return redirect()->back()->with('message', 'Jawaban berhasil ditambahkan');
    }

    public function jawabanEdit($id) {
        $jawabans = Jawaban::where('id', $id)->with('pertanyaan')->get();
        $penilaians = Penilaian::all();

        foreach ($jawabans as $value) {
            $value;
        }

        return view('admin.pages.jawabanEdit')->with(['jawabans' => $value, 'penilaians' => $penilaians]);
    }

    public function jawabanUpdate(Request $request) {
        $this->validate($request, [
            'jawaban' => 'required',
            'correct' => 'required',
            'penilaian_id' => 'required',
        ]);
        
        $penilaian = Penilaian::where('id', $request->penilaian_id)->select('id', 'benar', 'salah')->get();

        $point = '';
        $jawaban = '';
        if ($request->correct == 'Y') {
            $point = $penilaian->pluck('benar');
            $jawaban = 'Y';
        } else if ($request->correct == 'N') {
            $point = $penilaian->pluck('salah');
            $jawaban = 'N';
        }

        foreach ($point as $key => $value) {
            $form = array(
                'pertanyaan_id'     => $request->pertanyaan_id,
                'jawaban'           => $request->jawaban,
                'correct'           => $jawaban,
                'point'             => $value,
                'penilaian_id'      => $request->penilaian_id,
            );
        }

        Jawaban::whereId($request->hidden_id)->update($form);

        return redirect()->route('admin.jawaban', $request->pertanyaan_id)->with('message', 'Jawaban berhasil diupdate');
    }

    public function jawabanDelete($id) {
        $jawabans = Jawaban::findOrFail($id)->delete();

        return redirect()->back()->with('hapus', 'Jawaban berhasil dihapus');
    }

    // Penilaian
    public function penilaian() {
        $penilaians = Penilaian::orderBy('id', 'desc')->get();
        return view('admin.pages.penilaian')->with('penilaians', $penilaians);
    }

    public function penilaianAdd(Request $request) {
        $form = array(
            'nama_penilaian'    => $request->nama_penilaian,
            'level'             => $request->level,
            'benar'             => $request->benar,
            'salah'             => $request->salah
        );

        Penilaian::create($form);

        return response()->json(['success' => 'Penilaian berhasil ditambahkan']);
    }

    public function penilaianEdit($id) {
        $penilaians = Penilaian::findOrFail($id);

        return response()->json(['penilaian' => $penilaians]);
    }

    public function penilaianUpdate(Request $request, Penilaian $penilaian) {
        $form = array(
            'nama_penilaian'    => $request->nama_penilaian,
            'level'             => $request->level,
            'benar'             => $request->benar,
            'salah'             => $request->salah
        );

        Penilaian::whereId($request->hidden_id)->update($form);

        return response()->json(['success' => 'Penilaian berhasil diupdate']);
    }

    public function penilaianDelete($id) {
        $data = Penilaian::where('id', $id)->delete();
        // check data deleted or not
        if ($data == 1) {
            $success = true;
            $message = "Data deleted successfully";
        } else {
            $success = true;
            $message = "Data not found";
        }
        //  Return response
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }

    // Bank
    public function bank() {
        $banks = Bank::orderBy('id', 'desc')->get();

        return view('admin.pages.bank')->with('banks', $banks);
    }

    public function bankAdd(Request $request) {
        $form = array(
            'nama_bank'     => $request->nama_bank,
            'no_rekening'   => $request->no_rekening,
            'nama_pemilik'  => $request->nama_pemilik
        );

        Bank::create($form);

        return response()->json(['success' => 'Bank berhasil ditambahkan']);
    }

    public function bankEdit($id) {
        $banks = Bank::findOrFail($id);

        return response()->json(['bank' => $banks]);
    }

    public function bankUpdate(Request $request, Bank $bank) {
        $form = array(
            'nama_bank'     => $request->nama_bank,
            'no_rekening'   => $request->no_rekening,
            'nama_pemilik'  => $request->nama_pemilik
        );

        Bank::whereId($request->hidden_id)->update($form);

        return response()->json(['success' => 'Bank berhasil diupdate']);
    }

    public function bankDelete($id) {
        $data = Bank::where('id', $id)->delete();
        // check data deleted or not
        if ($data == 1) {
            $success = true;
            $message = "Data deleted successfully";
        } else {
            $success = true;
            $message = "Data not found";
        }
        //  Return response
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }

    public function bankActive($id) {
        $data = Bank::where('id', $id)->update([
            'is_active' => 1,
        ]);
        // check data deleted or not
        if ($data == 1) {
            $success = true;
            $message = "Data activated successfully";
        } else {
            $success = true;
            $message = "Data not found";
        }
        //  Return response
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }

    public function bankInActive($id) {
        $data = Bank::where('id', $id)->update([
            'is_active' => 0,
        ]);
        // check data deleted or not
        if ($data == 1) {
            $success = true;
            $message = "Data activated successfully";
        } else {
            $success = true;
            $message = "Data not found";
        }
        //  Return response
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }

    // Pembayaran
    public function pembayaran() {
        $data = Pembayaran::orderBy('id', 'desc')->with('user')->get();
        return view('admin.pages.pembayaran', [
            'datas' => $data
        ]);
    }

    public function pembayaranBukti($id) {
        $data = Pembayaran::findOrFail($id);

        return response()->json(['bukti' => $data]);
    }

    public function pembayaranConfirm($id) {
        $data = Pembayaran::where('id', $id)->update([
            'status' => 1,
        ]);
        // check data deleted or not
        if ($data == 1) {
            $success = true;
            $message = "Data activated successfully";
        } else {
            $success = true;
            $message = "Data not found";
        }
        //  Return response
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }

    public function pembayaranBatal($id) {
        $data = Pembayaran::where('id', $id)->update([
            'status' => 0,
        ]);
        // check data deleted or not
        if ($data == 1) {
            $success = true;
            $message = "Data activated successfully";
        } else {
            $success = true;
            $message = "Data not found";
        }
        //  Return response
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }

    public function sertifikat() {
        return view('admin.pages.sertifikat');
    }

    public function admin() {
        return view('admin.pages.admin', [
            'admins' => Admin::latest()->get()
        ]);
    }

    public function tambahAdmin(Request $request) {
        $form = [
            'name' => $request->nama_admin,
            'email' => $request->email,
            'password' => $request->password
        ];

        // dd($form);
        Admin::create($form);

        return response()->json(['success' => 'Admin berhasil ditambahakan']);
    }

    public function editAdmin($id) {
        $admin = Admin::find($id);
        return response()->json(['admin' => $admin]);
    }

    public function updateAdmin(Request $request) {
        $form = [
            'name' => $request->nama_admin,
            'email' => $request->email,
            'password' => $request->password
        ];

        $admins = Admin::whereId($request->hidden_id)->update($form);

        return response()->json(['success' => 'Admin berhasil diupdate']);
    }

    public function deleteAdmin($id) {
        $data = Admin::where('id', $id)->delete();
        // check data deleted or not
        if ($data == 1) {
            $success = true;
            $message = "Admin deleted successfully";
        } else {
            $success = true;
            $message = "Data not found";
        }
        //  Return response
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }

    public function excel() {
        $date = Carbon::now('Asia/Jakarta')->format('dmYhis');
        return Excel::download(new UserExportMapping(), 'data'.$date.'.xlsx');
    }
}
