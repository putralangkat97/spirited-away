@extends('user.layouts.master')

@section('content')
    <div class="container">
        <div class="col-xl-8 offset-xl-2">
            <div class="card-box">
                <h2>Konfirmasikan pembayaran anda disini.</h2>
                <p class="lead"><em><span class="text-danger"><b>*</b></span>Harap cantumkan bukti pembayaran agar proses konfirmasi dapat dilakukan secara cepat.</em></p>
                <div class="card-header">
                    <span>Form konfirmasi</span>
                </div>
                <form enctype="multipart/form-data" action="{{ route('user.konfirmasiAdd') }}" method="post">
                    @csrf
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="datepicker">Tanggal Transfer:</label>
                                    <input type="text" name="tanggal_transfer" id="datepicker" class="form-control" placeholder="Tanggal Transfer" required readonly>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="bank_id">Bank Tujuan:</label>
                                    <select name="bank_id" id="bank_id" class="form-control" required>
                                        @foreach ($banks as $bank)
                                        <option value="{{ $bank->id }}">{{ $bank->nama_bank }} - {{ $bank->no_rekening }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="pemilik_rekening">Rekening Atas Nama (Tujuan):</label>
                                    <input type="text" name="pemilik_rekening" id="pemilik_rekening" class="form-control" placeholder="Rekening Atas Nama" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="nominal">Nominal Transfer (Masukkan hanya angka, cth: 150000):</label>
                                    <input type="text" name="nominal" id="nominal" class="form-control" placeholder="0000" required oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="image">Upload bukti Transfer:</label>
                                    <input type="file" name="image" id="image" class="form-control" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-2 col-md-12 col-12 offset-lg-5">
                                <button type="submit" class="btn btn-primary">Konfirmasi <i class="mdi mdi-arrow-right"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
<script>
    $(document).ready(function () {
        $('#datepicker').datepicker({
            uiLibrary: 'bootstrap',
            format: 'yyyy-mm-dd'
        });
    });
</script>
@endsection