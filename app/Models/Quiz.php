<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    protected $guarded = [];

    public function olimpiade() {
        return $this->belongsTo('App\Models\Olimpiade', 'olimpiade_id');
    }

    public function pertanyaans() {
        return $this->hasMany('App\Models\Pertanyaan', 'quiz_id');
    }
}
