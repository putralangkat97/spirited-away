<nav class="uk-navbar-container uk-letter-spacing-small uk-text-bold fixed-top">
    <div class="uk-container uk-container-large">
        <div class="uk-position-z-index" data-uk-navbar>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-3 col-sm-3">
                        <a class="uk-navbar-item uk-logo" href="/"><img src="{{ asset('img/posi.png') }}" style="max-width: 230px;"></a>
                    </div>
                    <div class="col-lg-5 col-sm-6"></div>
                    <div class="col-lg-4 col-sm-3">
                        <div class="uk-navbar-item">
                            @if (Route::has('login'))
                                <div class="top-right links">
                                    @auth
                                    <a href="/dashboard">
                                        @if(Auth::user()->jenis_kelamin == 'Laki-Laki')
                                    <img src="{{ asset('img/male.png') }}" alt="User image" width="40">
                                        @elseif(Auth::user()->jenis_kelamin == 'Perempuan')
                                    <img src="{{ asset('img/female.png') }}" alt="User image" width="40">
                                        @endif
                                        {{ Auth::user()->username }}
                                    </a>
                                    |
                                    <a href="{{ route('user.logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('user.logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                    @else
                                    <div><a class="uk-button uk-button-success-outline" href="/login">Sign In</a></div>
                                    @endauth
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>
